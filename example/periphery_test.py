#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    Test if your periphery (e.g. SCC powersupply) is set up correctly
'''

import os
import time
import logging

import basil


settings = {
    'VDDA': 1.25,
    'VDDD': 1.25,
    
    'VDDA_cur_lim': 0.7,
    'VDDD_cur_lim': 0.7
    }


def init_periphery():
    proj_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
    periphery_conf = os.path.join(proj_dir, 'bdaq53' + os.sep + 'periphery.yaml')
    periphery = basil.dut.Dut(periphery_conf)
    periphery.init()
    
    for ps in periphery:
        if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
            continue
        logging.info('Found channel %s' % (ps.name))
        
    return periphery


def power_on(periphery, **settings):
    for ps in periphery:
        if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
            continue
        
        ps.set_voltage(settings[ps.name])
        ps.set_current_limit(settings[ps.name + '_cur_lim'])
        
        ps.set_enable(on=True)
        logging.info('Set channel %s to %1.3f and turned on' % (ps.name, settings[ps.name]))
        
        time.sleep(1)


def get_power(periphery):
    currents = {}

    for ps in periphery:
        if not isinstance(ps, basil.RL.FunctionalRegister.FunctionalRegister):
            continue

        currents[ps.name] = ps.get_current()
        
    text = ''
    for key, value in currents.iteritems():
        text += ', on ' + key + '=' + str(value) + 'A'
    logging.info('Current %s' % (text[2:]))
        
    return currents
        
if __name__ == '__main__':
    periphery = init_periphery()
#     power_on(periphery, **settings)
    get_power(periphery)
