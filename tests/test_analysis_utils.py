#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib  # workaround
import logging
import os
import unittest

import tables as tb

from bdaq53.analysis import analysis
from bdaq53.analysis.plotting import Plotting
import bdaq53.analysis.analysis_utils as au
import utils

import bdaq53
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data',
                                           'fixtures'))


class TestAnalysisUtils(unittest.TestCase):

    @classmethod
    def tearDownClass(cls):
        os.remove(os.path.join(data_folder,
                               'threshold_scan_fitted.h5'))
        os.remove(os.path.join(data_folder,
                               'threshold_scan_inv_fitted.h5'))
        os.remove(os.path.join(data_folder,
                               'analog_scan_chunked_interpreted.h5'))
        os.remove(os.path.join(data_folder,
                               'ext_trigger_scan_replayed.h5'))

    def test_replay_function(self):
        ''' Test triggered data replay function '''
        from bdaq53.scans.scan_eudaq import replay_triggered_data

        raw_data_file = os.path.join(data_folder, 'ext_trigger_scan.h5')
        raw_data_file_replayed = raw_data_file[:-3] + '_replayed.h5'

        with tb.open_file(raw_data_file_replayed, 'w') as out_file:
            raw_data_earray = out_file.create_earray(out_file.root,
                                                     name='raw_data',
                                                     atom=tb.UIntAtom(),
                                                     shape=(0,),
                                                     title='raw_data',
                                                     filters=tb.Filters(complib='blosc', complevel=5, fletcher32=False))
            for dat in replay_triggered_data(raw_data_file, real_time=False):
                raw_data_earray.append(dat)

        # Replayed raw data array has to be the same than original
        equal, msg = utils.compare_h5_files(raw_data_file,
                                            raw_data_file_replayed,
                                            node_names=('raw_data', ))
        self.assertTrue(equal, msg=msg)

    def test_thr_scan_fit(self):
        ''' Test fitting of threshold scan data. '''
        with tb.open_file(os.path.join(data_folder, 'threshold_scan_fitted_result.h5')) as in_file:
            with tb.open_file(os.path.join(data_folder, 'threshold_scan_fitted.h5'), 'w') as out_file:
                config = au.ConfigDict(in_file.root.configuration.run_config[:])
                start = config['VCAL_HIGH_start']
                stop = config['VCAL_HIGH_stop']
                step = config['VCAL_HIGH_step']
                n_injections = config['n_injections']
                hist_scurve = in_file.root.HistSCurve[:]
                scan_param_range = range(start, stop, step)

                threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(hist_scurve,
                                                                                scan_param_range=scan_param_range,
                                                                                n_injections=n_injections)
                filters = tb.Filters(complib='blosc',
                                     complevel=5,
                                     fletcher32=False)
                out_file.create_carray(out_file.root,
                                       name='ThresholdMap',
                                       title='Threshold Map',
                                       obj=threshold_map,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='NoiseMap',
                                       title='Noise Map',
                                       obj=noise_map,
                                       filters=filters)

                out_file.create_carray(out_file.root,
                                       name='Chi2Map',
                                       title='Chi2 / ndf Map',
                                       obj=chi2_map,
                                       filters=filters)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_fitted.h5'),
            os.path.join(data_folder, 'threshold_scan_fitted_result.h5'),
            node_names=['NoiseMap', 'ThresholdMap', 'Chi2Map'],
            exact=False,
            # Allow Threshold/Noise to be 1% off due to fit
            rtol=0.01)
        self.assertTrue(data_equal, msg=error_msg)

    def test_thr_scan_inv_fit(self):
        ''' Test fitting of threshold scan data with x-inverted curves. '''
        with tb.open_file(os.path.join(data_folder, 'global_threshold_tuning_interpreted_result.h5')) as in_file:
            with tb.open_file(os.path.join(data_folder, 'threshold_scan_inv_fitted.h5'), 'w') as out_file:
                config = au.ConfigDict(in_file.root.configuration.run_config[:])
                start = config['VTH_start']
                stop = config['VTH_stop']
                step = config['VTH_step']
                n_injections = config['n_injections']
                hist_scurve = in_file.root.HistSCurve[:]
                scan_param_range = range(start, stop, -1 * step)

                threshold_map, noise_map, chi2_map = au.fit_scurves_multithread(hist_scurve,
                                                                                scan_param_range=scan_param_range,
                                                                                n_injections=n_injections,
                                                                                invert_x=True)
                filters = tb.Filters(complib='blosc',
                                     complevel=5,
                                     fletcher32=False)
                out_file.create_carray(out_file.root,
                                       name='ThresholdMap',
                                       title='Threshold Map',
                                       obj=threshold_map,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='NoiseMap',
                                       title='Noise Map',
                                       obj=noise_map,
                                       filters=filters)

                out_file.create_carray(out_file.root,
                                       name='Chi2Map',
                                       title='Chi2 / ndf Map',
                                       obj=chi2_map,
                                       filters=filters)

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_inv_fitted.h5'),
            os.path.join(data_folder, 'threshold_scan_inv_fitted_result.h5'),
            node_names=['NoiseMap', 'ThresholdMap', 'Chi2Map'],
            exact=False,
            # Allow Threshold/Noise to be 1% off due to fit
            rtol=0.01)
        self.assertTrue(data_equal, msg=error_msg)

    def test_helper_func_ana(self):
        ''' Test helper function for quick in-scan raw data analysis.
        '''
        chunk_size = 100000
        with tb.open_file(os.path.join(data_folder, 'analog_scan.h5')) as in_file:
            with tb.open_file(os.path.join(data_folder, 'analog_scan_chunked_interpreted.h5'), 'w') as out_file:
                raw_data = in_file.root.raw_data

                n_raw = raw_data.shape[0]

                (hits, hist_occ, hist_tot, hist_rel_bcid, _,
                 hist_event_status, hist_bcid_error) = au.init_outs(n_raw * 4,
                                                                    n_scan_params=1)

                for i in range(0, n_raw, chunk_size):
                    data = au.analyze_chunk(rawdata=raw_data[i:i + chunk_size],
                                            return_hists=('hist_occ', 'hist_tot',
                                                          'hist_rel_bcid',
                                                          'hist_event_status',
                                                          'hist_bcid_error'),
                                            return_hits=True)
                    hist_occ += data['hist_occ']
                    hist_tot += data['hist_tot']
                    hist_rel_bcid += data['hist_rel_bcid']
                    hist_event_status += data['hist_event_status']
                    hist_bcid_error += data['hist_bcid_error']

                filters = tb.Filters(complib='blosc',
                                     complevel=5,
                                     fletcher32=False)
                out_file.create_carray(out_file.root,
                                       name='HistOcc',
                                       title='Occupancy Histogram',
                                       obj=hist_occ,
                                       filters=filters)
                out_file.create_carray(out_file.root,
                                       name='HistTot',
                                       title='ToT Histogram',
                                       obj=hist_tot,
                                       filters=filters)

        data_equal, error_msg = utils.compare_h5_files(os.path.join(data_folder, 'analog_scan_chunked_interpreted.h5'),
                                                       os.path.join(data_folder, 'analog_scan_interpreted_result.h5'),
                                                       # Only hists that do not need proper event building can be tested
                                                       node_names=('HistOcc', 'HistTot'))
        self.assertTrue(data_equal, msg=error_msg)


if __name__ == '__main__':
    unittest.main()
