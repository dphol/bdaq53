#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

import zlib  # workaround
import logging
import os
import unittest

import tables as tb

from bdaq53.analysis import analysis
from bdaq53.analysis.plotting import Plotting
import bdaq53.analysis.analysis_utils as au
import utils

import bdaq53
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data',
                                           'fixtures'))


class TestAnalysis(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        super(TestAnalysis, cls).setUpClass()
        plt_logger = logging.getLogger('Plotting')
        cls._plt_log_handler = utils.MockLoggingHandler(level='DEBUG')
        plt_logger.addHandler(cls._plt_log_handler)
        cls.plt_log_messages = cls._plt_log_handler.messages

        ana_logger = logging.getLogger('Analysis')
        cls._ana_log_handler = utils.MockLoggingHandler(level='DEBUG')
        ana_logger.addHandler(cls._ana_log_handler)
        cls.ana_log_messages = cls._ana_log_handler.messages

    @classmethod
    def tearDownClass(cls):
        test_files = ['digital_scan', 'analog_scan', 'threshold_scan',
                      'dac_linearity_test', 'global_threshold_tuning',
                      'local_threshold_tuning']
        for test_file in test_files:
            os.remove(os.path.join(data_folder,
                                   test_file + '_interpreted.h5'))
        os.remove(os.path.join(data_folder, 'analog_scan_clustered.h5'))
        os.remove(os.path.join(data_folder,
                               'analog_scan_clustered_prop.h5'))
        os.remove(os.path.join(data_folder,
                               'threshold_scan_interpreted.pdf'))
        os.remove(os.path.join(data_folder,
                               'last_scan.pdf'))

    def test_dig_scan_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_chunked_ana(self):
        ''' Test analysis of digital scan data '''
        raw_data_file = os.path.join(data_folder, 'digital_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True, chunk_size=99991) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'digital_scan_interpreted.h5'),
            os.path.join(data_folder, 'digital_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_ana_scan_ana(self):
        ''' Test analysis of analog scan data '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_interpreted.h5'),
            os.path.join(data_folder, 'analog_scan_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_thr_scan_inter(self):
        ''' Test interpretation of threshold scan data '''
        raw_data_file = os.path.join(data_folder, 'threshold_scan.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'threshold_scan_interpreted.h5'),
            os.path.join(data_folder, 'threshold_scan_interpreted_result.h5'),
            node_names=['HistOcc', 'HistTot', 'HistSCurve'],
            exact=True)
        self.assertTrue(data_equal, msg=error_msg)

        with Plotting(analyzed_data_file=a.analyzed_data_file, pdf_file=None,
                      level='preliminary', qualitative=False, internal=False,
                      save_single_pdf=False, save_png=False) as p:
            p.create_standard_plots()

        # Check that now errors are logged (= all plots created)
        self.assertFalse(self.plt_log_messages['error'])

    def test_glob_tun_ana(self):
        ''' Test analysis of global threshold tuning '''
        raw_data_file = os.path.join(data_folder, 'global_threshold_tuning.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'global_threshold_tuning_interpreted.h5'),
            os.path.join(data_folder, 'global_threshold_tuning_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_loc_tun_ana(self):
        ''' Test analysis of local threshold tuning '''
        raw_data_file = os.path.join(data_folder, 'local_threshold_tuning.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'local_threshold_tuning_interpreted.h5'),
            os.path.join(data_folder, 'local_threshold_tuning_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_full(self):
        ''' Test cluster results against fixture '''
        with analysis.Analysis(
                raw_data_file=os.path.join(data_folder,
                                           'analog_scan.h5'),
                analyzed_data_file=os.path.join(
                    data_folder, 'analog_scan_clustered.h5'),
                cluster_hits=True, store_hits=True) as a:
            a.analyze_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'analog_scan_clustered.h5'),
            os.path.join(data_folder, 'analog_scan_clustered_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_clustering_properties(self):
        ''' Check validity of cluster results '''
        raw_data_file = os.path.join(data_folder, 'analog_scan.h5')
        analyzed_data_file = os.path.join(
            data_folder, 'analog_scan_clustered_prop.h5')
        with analysis.Analysis(raw_data_file=raw_data_file,
                               analyzed_data_file=analyzed_data_file,
                               store_hits=True, cluster_hits=True) as a:
            a.analyze_data()

        # Deduce properties from hit table
        with tb.open_file(analyzed_data_file) as in_file:
            hits = in_file.root.Hits[:]
            # Omit late hits do prevent double counting
            # Only ToT < 14 is clustered
            hits = hits[hits['tot'] < 14]
            n_hits = hits.shape[0]
            tot_sum = hits['tot'].sum()

        with tb.open_file(analyzed_data_file) as in_file:
            self.assertEqual(in_file.root.Cluster[:]['size'].sum(), n_hits)
            self.assertEqual(in_file.root.Cluster[:]['tot'].sum(), tot_sum)

    def test_dac_linearity_ana(self):
        raw_data_file = os.path.join(data_folder, 'dac_linearity_test.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_adc_data()

        data_equal, error_msg = utils.compare_h5_files(
            os.path.join(data_folder, 'dac_linearity_test_interpreted.h5'),
            os.path.join(data_folder, 'dac_linearity_test_interpreted_result.h5'))
        self.assertTrue(data_equal, msg=error_msg)

    def test_emtpy_data(self):
        ''' Test for useful error message for empty data
        '''
        raw_data_file = os.path.join(data_folder, 'empty_data.h5')
        with analysis.Analysis(raw_data_file=raw_data_file, store_hits=True) as a:
            a.analyze_data()

        self.assertTrue(any('Data is empty' in s for s in self.ana_log_messages['warning']))


if __name__ == '__main__':
    unittest.main()
