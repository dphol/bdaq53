#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
This test case needs a working eudaq 1.7 installation and the
eudaq/python folder added to the python path, see:
https://gitlab.cern.ch/silab/bdaq53/wikis/Eudaq-integration

The installation has to take place at std. location (bin/lib in
eudaq folder)

Otherwise the unit tests are all skipped.
'''

import zlib  # workaround
import os
import time
import unittest
import threading
import subprocess
import sys
from Queue import Queue, Empty

import psutil

import bdaq53
bdaq53_path = os.path.dirname(bdaq53.__file__)
data_folder = os.path.abspath(os.path.join(bdaq53_path, '..', 'data',
                                           'fixtures'))


def run_process_with_queue(command, arguments=None):
    if os.name == 'nt':
        creationflags = subprocess.CREATE_NEW_PROCESS_GROUP
    else:
        creationflags = 0
    args = [command]
    if arguments:
        args += [str(a) for a in arguments]
    ON_POSIX = 'posix' in sys.builtin_module_names
    p = subprocess.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                         bufsize=1, close_fds=ON_POSIX, creationflags=creationflags)
    print_queue = Queue()
    t = threading.Thread(target=enqueue_output, args=(p.stdout, print_queue))
    t.daemon = True  # thread dies with the program
    t.start()

    return p, print_queue


def kill(proc):
    ''' Kill process by id, including subprocesses.

        Works for Linux and Windows
    '''
    process = psutil.Process(proc.pid)
    for child_proc in process.children(recursive=True):
        child_proc.kill()
    process.kill()


def eurun_control(min_connections=2, run_time=5):
    ''' Start a EUDAQ run control instance

    Waits for min_connections to send configure command.
    Start the run for run_time and then send EOR.
    '''
    from PyEUDAQWrapper import PyRunControl
    prc = PyRunControl("44000")
    while prc.NumConnections < min_connections:
        time.sleep(1)
        print "Number of active connections: ", prc.NumConnections
    # Load configuration file
    prc.Configure("TestConfig.conf")
    # Wait that the connections can receive the config
    time.sleep(2)
    # Wait until all connections answer with 'OK'
    while not prc.AllOk:
        time.sleep(0.5)
        print "Successfullly configured!"
    # start the run
    prc.StartRun()
    # Wait for run start
    while not prc.AllOk:
        time.sleep(0.5)
    # Main run loop
    time.sleep(run_time)
    # Stop run
    prc.StopRun()
    time.sleep(2)


def enqueue_output(out, queue):
    ''' https://stackoverflow.com/questions/375427/
    non-blocking-read-on-a-subprocess-pipe-in-python '''
    for line in iter(out.readline, b''):
        queue.put(line)
    out.close()


def queue_to_string(queue, max_lines=1000):
    return_str = ''
    for _ in range(max_lines):
        try:
            line = queue.get_nowait()
        except Empty:
            break
        else:  # got line
            return_str += line + '\n'
    return return_str


class TestEudaq(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        try:
            import PyEUDAQWrapper
            eudaq_wrapper_path = os.path.dirname(PyEUDAQWrapper.__file__)
            cls.eudaq_bin_folder = os.path.abspath(os.path.join(
                eudaq_wrapper_path, '..', 'bin'))
        except ImportError:
            raise unittest.SkipTest('No working EUDAQ installation, skip tests!')

    def test_replay_data(self):
        ''' Test the communication with replayed data.

            Starts a complete EUDAQ data taking scenario:
                Run control + TestDataConverter + bdaq53 producer

            Success of this test is checked by stdout inspection.
        '''
        t_rc = threading.Thread(target=eurun_control,
                                kwargs={"min_connections": 2})
        t_rc.start()
        time.sleep(1)
        data_coll_process, data_coll_q = run_process_with_queue(
            command=os.path.join(self.eudaq_bin_folder, 'TestDataCollector.exe'))
        time.sleep(1)
        bdaq53_prod_process, bdaq53_prod_q = run_process_with_queue(
            command='bdaq53_eudaq',
            arguments=['--replay',
                       '%s' % os.path.join(data_folder, 'ext_trigger_scan.h5'),
                       '--delay', '0.001'])
        time.sleep(10)

        t_rc.join()

        time.sleep(5)

        # Read up to 1000 lines from Data Collector output
        coll_output = queue_to_string(data_coll_q)
        prod_output = queue_to_string(bdaq53_prod_q)

        # Subprocesses have to be killed since no terminate signal is available
        # from euRun wrapped with python
        kill(data_coll_process)
        kill(bdaq53_prod_process)

        # Check configurig of entities
        self.assertTrue('Configuring' in coll_output)
        self.assertTrue('Configured' in coll_output)
        self.assertTrue('Received Configuration' in prod_output)

        # Check event building from replayed data
        self.assertTrue('Complete Event' in coll_output)

        # Check stop run signal received
        self.assertTrue('Stop Run' in coll_output)
        self.assertTrue('Stop Run received' in prod_output)


if __name__ == '__main__':
    unittest.main()
