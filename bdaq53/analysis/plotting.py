#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    BDAQ53 plotting class

    This class can be used standalone to reproduce plots from interpreted data files generated with BDAQ53.
    To install prerequisutes in an (Ana- or Mini-) Conda environment, run
        conda install --yes numpy scipy pytables matplotlib tqdm blosc

    Standalone usage:
        At the bottom of the script,
        - enter the path to the interpreted data file,
        - select the appropriate flags,
        - select which plots to generate
        and run the script.
'''

import zlib  # workaround for segmentation fault on tables import
import numpy as np
import math
import logging
import shutil
import os
import matplotlib

import tables as tb
import matplotlib.pyplot as plt

from collections import OrderedDict
from scipy.optimize import curve_fit
from matplotlib.figure import Figure
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from mpl_toolkits.axes_grid1 import make_axes_locatable
from matplotlib import colors, cm
from matplotlib.backends.backend_pdf import PdfPages


logging.basicConfig(
    format="%(asctime)s - [%(name)-8s] - %(levelname)-7s %(message)s")
loglevel = logging.INFO


ELECTRON_CONVERSION = {'slope': 10.02, 'offset': 64}   # [slope] = Electrons / Delta VCAL; [offset] = Electrons
TITLE_COLOR = '#07529a'
OVERTEXT_COLOR = '#07529a'


class Plotting(object):
    def __init__(self, analyzed_data_file, pdf_file=None, level='preliminary', qualitative=False, internal=False, save_single_pdf=False, save_png=False):
        self.logger = logging.getLogger('Plotting')
        self.logger.setLevel(loglevel)

        self.plot_cnt = 0
        self.save_single_pdf = save_single_pdf
        self.save_png = save_png
        self.level = level
        self.qualitative = qualitative
        self.internal = internal
        self.clustered = False

        if pdf_file is None:
            self.filename = '.'.join(
                analyzed_data_file.split('.')[:-1]) + '.pdf'
        else:
            self.filename = pdf_file
        self.out_file = PdfPages(self.filename)

        if isinstance(analyzed_data_file, str):
            in_file = tb.open_file(analyzed_data_file, 'r')
            root = in_file.root
        else:
            root = analyzed_data_file

        self.run_config = dict(root.configuration.run_config[:])
        self.dacs = dict(root.configuration.dacs[:])
        self.enable_mask = self._mask_disabled_pixels(
            root.configuration.enable_mask[:], self.run_config)
        self.tdac_mask = root.configuration.TDAC_mask[:]

        if self.run_config['scan_id'] not in ['dac_linearity_test']:
            self.HistEventStatus = root.HistEventStatus[:]
            self.HistOcc = root.HistOcc[:]
            self.HistTot = root.HistTot[:]
            self.HistRelBCID = root.HistRelBCID[:]
            self.HistBCIDError = root.HistBCIDError[:]
            if self.run_config['scan_id'] in ['threshold_scan', 'global_threshold_tuning', 'local_threshold_tuning']:
                self.HistSCurve = root.HistSCurve[:]
                self.ThresholdMap = root.ThresholdMap[:, :]
                self.Chi2Map = root.Chi2Map[:, :]
                self.NoiseMap = root.NoiseMap[:]

        try:
            self.Hits = root.Hits[:]
        except:
            self.Hits = None

        try:
            self.multimeter_data = root.multimeter_data[:]
        except:
            self.multimeter_data = None

        try:
            self.Cluster = root.Cluster[:]
            self.HistClusterSize = root.HistClusterSize[:]
            self.HistClusterShape = root.HistClusterShape[:]
            self.HistClusterTot = root.HistClusterTot[:]
            self.clustered = True
        except:
            pass

        try:
            in_file.close()
        except:
            pass

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        if self.out_file is not None and isinstance(self.out_file, PdfPages):
            self.logger.info('Closing output PDF file: %s',
                             str(self.out_file._file.fh.name))
            self.out_file.close()
            shutil.copyfile(self.filename, os.path.join(
                os.path.split(self.filename)[0], 'last_scan.pdf'))

    ''' User callable plotting functions '''

    def create_standard_plots(self):
        self.logger.info('Creating selected plots...')
        if self.run_config['scan_id'] == 'dac_linearity_test':
            self.create_parameter_page()
            self.create_dac_linearity_plot()
        else:
            self.create_parameter_page()
            self.create_event_status_plot()
            self.create_occupancy_map()
            self.create_tot_plot()
            self.create_rel_bcid_plot()
            self.create_bcid_error_plot()
            if self.run_config['scan_id'] in ['analog_scan', 'threshold_scan', 'noise_occupancy_scan', 'global_threshold_tuning']:
                self.create_tdac_plot()
            if self.run_config['scan_id'] == 'threshold_scan':
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_stacked_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.run_config['scan_id'] == 'global_threshold_tuning':
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.run_config['scan_id'] == 'local_threshold_tuning':
                self.create_scurves_plot()
                self.create_threshold_plot()
                self.create_threshold_map()
                self.create_noise_plot()
                self.create_noise_map()
            if self.clustered:
                self.create_cluster_tot_plot()
                self.create_cluster_shape_plot()
                self.create_cluster_size_plot()

    def create_parameter_page(self):
        try:
            self._plot_parameter_page()
        except:
            self.logger.error('Could not create parameter page!')

    def create_event_status_plot(self):
        try:
            self._plot_event_status(hist=self.HistEventStatus.T)
        except:
            self.logger.error('Could not create event status plot!')

    def create_bcid_error_plot(self):
        try:
            self._plot_bcid_error(hist=self.HistBCIDError.T)
        except:
            self.logger.error('Could not create BCID error plot!')

    def create_occupancy_map(self):
        try:
            if self.run_config['scan_id'] in ['threshold_scan', 'global_threshold_tuning', 'local_threshold_tuning']:
                title = 'Integrated occupancy'
            else:
                title = 'Occupancy'

            self._plot_occupancy(hist=np.ma.masked_array(
                self.HistOcc[:], self.enable_mask).T, suffix='occupancy', title=title)
        except:
            self.logger.error('Could not create occupancy map!')

    def create_tot_plot(self):
        try:
            self._plot_tot(hist=self.HistTot.T)
        except:
            self.logger.error('Could not create tot plot!')

    def create_rel_bcid_plot(self):
        try:
            self._plot_relative_bcid(hist=self.HistRelBCID.T)
        except:
            self.logger.error('Could not create relative BCID plot!')

    def create_scurves_plot(self, scan_parameter_name='Scan parameter'):
        try:
            if self.run_config['scan_id'] == 'threshold_scan':
                scan_parameter_name = '$\Delta$ VCAL'
                electron_axis = True
                scan_parameter_range = [v - int(self.run_config['VCAL_MED']) for v in range(int(self.run_config['VCAL_HIGH_start']),
                                                                                            int(
                                                                                                self.run_config['VCAL_HIGH_stop']) + 1,
                                                                                            int(self.run_config['VCAL_HIGH_step']))]
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                scan_parameter_name = self.run_config['VTH_name']
                electron_axis = False
                scan_parameter_range = range(int(self.run_config['VTH_start']),
                                             int(self.run_config['VTH_stop']),
                                             -1 * int(self.run_config['VTH_step']))
            elif self.run_config['scan_id'] == 'local_threshold_tuning':
                min_tdac, max_tdac, _ = self._get_tdac_range()
                scan_parameter_name = 'TDAC'
                electron_axis = False
                scan_parameter_range = range(min_tdac, max_tdac + 2)

            self._plot_scurves(scurves=self.HistSCurve.T,
                               scan_parameters=scan_parameter_range,
                               electron_axis=electron_axis,
                               scan_parameter_name=scan_parameter_name)
        except:
            self.logger.error('Could not create scurve plot!')

    def create_threshold_plot(self, scan_parameter_name='Scan parameter'):
        try:
            if self.run_config['scan_id'] == 'threshold_scan':
                plot_range = [v - int(self.run_config['VCAL_MED']) for v in range(int(self.run_config['VCAL_HIGH_start']),
                                                                                  int(self.run_config['VCAL_HIGH_stop']) + 1,
                                                                                  int(self.run_config['VCAL_HIGH_step']))]
                scan_parameter_name = '$\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                plot_range = range(int(self.run_config['VTH_stop']),
                                   int(self.run_config['VTH_start']),
                                   int(self.run_config['VTH_step']))
                scan_parameter_name = self.run_config['VTH_name']
                electron_axis = False
            elif self.run_config['scan_id'] == 'local_threshold_tuning':
                min_tdac, max_tdac, _ = self._get_tdac_range()
                plot_range = range(min_tdac, max_tdac + 2)
                scan_parameter_name = 'TDAC'
                electron_axis = False

            self._plot_distribution(self.ThresholdMap.T,
                                    plot_range=plot_range,
                                    electron_axis=electron_axis,
                                    x_axis_title=scan_parameter_name,
                                    title='Threshold distribution',
                                    suffix='threshold_distribution')
        except:
            self.logger.error('Could not create threshold plot!')

    def create_stacked_threshold_plot(self, scan_parameter_name='Scan parameter'):
        try:
            min_tdac, max_tdac, range_tdac = self._get_tdac_range()
            if self.run_config['scan_id'] == 'threshold_scan':
                plot_range = [v - int(self.run_config['VCAL_MED']) for v in range(int(self.run_config['VCAL_HIGH_start']),
                                                                                  int(self.run_config['VCAL_HIGH_stop']) + 1,
                                                                                  int(self.run_config['VCAL_HIGH_step']))]
                scan_parameter_name = '$\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                plot_range = range(int(self.run_config['VTH_stop']),
                                   int(self.run_config['VTH_start']),
                                   int(self.run_config['VTH_step']))
                scan_parameter_name = self.run_config['VTH_name']
                electron_axis = False

            self._plot_stacked_threshold(data=self.ThresholdMap.T,
                                         tdac_mask=self.tdac_mask.T,
                                         plot_range=plot_range,
                                         electron_axis=electron_axis,
                                         x_axis_title=scan_parameter_name,
                                         title='Threshold distribution',
                                         suffix='threshold_distribution',
                                         min_tdac=min_tdac,
                                         max_tdac=max_tdac,
                                         range_tdac=range_tdac)
        except:
            self.logger.error('Could not create stacked threshold plot!')

    def create_threshold_map(self):
        try:
            mask = self.enable_mask
            sel = self.Chi2Map[:] > 0.  # Mask not converged fits (chi2 = 0)
            mask[~sel] = True

            self._plot_occupancy(hist=np.ma.masked_array(self.ThresholdMap, mask).T,
                                 electron_axis=True,
                                 z_label='Threshold',
                                 title='Threshold',
                                 show_sum=False,
                                 suffix='threshold_map')
        except:
            self.logger.error('Could not create threshold map!')

    def create_noise_plot(self, scan_parameter_name='Scan parameter'):
        try:
            if self.run_config['scan_id'] == 'threshold_scan':
                scan_parameter_name = '$\Delta$ VCAL'
                electron_axis = True
            elif self.run_config['scan_id'] == 'global_threshold_tuning':
                scan_parameter_name = self.run_config['VTH_name']
                electron_axis = False
            elif self.run_config['scan_id'] == 'local_threshold_tuning':
                scan_parameter_name = 'TDAC'
                electron_axis = False

            self._plot_distribution(self.NoiseMap.T,
                                    title='Noise distribution',
                                    electron_axis=electron_axis,
                                    x_axis_title=scan_parameter_name,
                                    y_axis_title='# of hits',
                                    suffix='noise_distribution')
        except:
            self.logger.error('Could not create noise plot!')

    def create_noise_map(self):
        try:
            mask = self.enable_mask
            sel = self.Chi2Map[:] > 0.  # Mask not converged fits (chi2 = 0)
            mask[~sel] = True

            self._plot_occupancy(hist=np.ma.masked_array(self.NoiseMap, mask).T,
                                 electron_axis=True,
                                 z_label='Noise',
                                 z_max='median',
                                 title='Noise',
                                 show_sum=False,
                                 suffix='noise_map')
        except:
            self.logger.error('Could not create noise map!')

    def create_tdac_plot(self):
        try:
            min_tdac, max_tdac, _ = self._get_tdac_range()
            self._plot_distribution(self.tdac_mask.T,
                                    plot_range=range(min_tdac, max_tdac + 2),
                                    title='TDAC distribution',
                                    x_axis_title='TDAC',
                                    y_axis_title='# of hits',
                                    suffix='tdac_distribution')
        except:
            self.logger.error('Could not create TDAC plot!')

    def create_chi2_map(self):
        try:
            mask = self.enable_mask
            chi2 = self.Chi2Map[:]
            sel = chi2 > 0.  # Mask not converged fits (chi2 = 0)
            mask[~sel] = True

            self._plot_occupancy(hist=np.ma.masked_array(chi2, mask).T,
                                 z_label='Chi2/ndf.',
                                 z_max='median',
                                 title='Chi2 over ndf of S-Curve fits',
                                 show_sum=False,
                                 suffix='chi2_map')
        except:
            self.logger.error('Could not create chi2 map!')

    def create_dac_linearity_plot(self, adc_ref=0.8, I_ref=4, y_title=None):
        try:
            self._plot_dac_linearity(data=self.Hits,
                                     title=self.run_config['DAC'] +
                                     ' Linearity',
                                     y_axis_title='ADC [LSB]',
                                     x_axis_title=self.run_config['DAC'])
        except:
            self.logger.error('Could not create DAC linearity plot from ADC data!')

        try:
            if self.run_config['type'] == 'U':
                y_axis_title = 'Voltage [mV]'
            else:
                y_axis_title = 'Current [$\mu$A]'
            if y_title:
                y_axis_title = y_title

            self._plot_dac_linearity(data=self.multimeter_data,
                                     title=self.run_config['DAC'] +
                                     ' Linearity',
                                     y_axis_title=y_axis_title,
                                     x_axis_title=self.run_config['DAC'] +
                                     ' [LSB]',
                                     second_axis=False,
                                     suffix='linearity_2',
                                     adc_ref=adc_ref,
                                     I_ref=I_ref)

            if self.run_config['value_step'] == '1':
                self._plot_dac_non_linearity(data=self.multimeter_data,
                                             title=self.run_config['DAC'] +
                                             ' Non-Linearity',
                                             typ='INL',
                                             y_axis_title='INL [LSB]',
                                             x_axis_title=self.run_config['DAC'] +
                                                 ' [LSB]',
                                             suffix='INL')

                self._plot_dac_non_linearity(data=self.multimeter_data,
                                             title=self.run_config['DAC'] +
                                             ' Non-Linearity',
                                             typ='DNL',
                                             y_axis_title='DNL [LSB]',
                                             x_axis_title=self.run_config['DAC'] +
                                                 ' [LSB]',
                                             suffix='DNL')
        except:
            self.logger.error(
                'Could not create DAC linearity plot from multimeter data!')

    def create_cluster_size_plot(self):
        try:
            self._plot_cl_size(self.HistClusterSize)
        except:
            self.logger.error('Could not create cluster size plot!')

    def create_cluster_tot_plot(self):
        try:
            self._plot_cl_tot(self.HistClusterTot)
        except:
            self.logger.error('Could not create cluster TOT plot!')

    def create_cluster_shape_plot(self):
        try:
            self._plot_cl_shape(self.HistClusterShape)
        except:
            self.logger.error('Could not create cluster shape plot!')

    ''' Internal functions '''

    def _get_tdac_range(self):
        start_column = int(self.run_config['start_column'])
        if start_column >= 264:
            min_tdac = -15
            max_tdac = 16
            range_tdac = 32
        else:
            min_tdac = 0
            max_tdac = 15
            range_tdac = 16
        return min_tdac, max_tdac, range_tdac

    def _mask_disabled_pixels(self, enable_mask, run_config):
        mask = np.invert(enable_mask)

        mask[:int(run_config.get('start_column', 0)), :] = True
        mask[int(run_config.get('stop_column', 400)):, :] = True
        mask[:, :int(run_config.get('start_row', 0))] = True
        mask[:, int(run_config.get('stop_row', 192)):] = True

        return mask

    def _save_plots(self, fig, suffix=None, tight=False):
        increase_count = False
        bbox_inches = 'tight' if tight else ''
        if suffix is None:
            suffix = str(self.plot_cnt)

        if not self.out_file:
            fig.show()
        else:
            self.out_file.savefig(fig, bbox_inches=bbox_inches)
        if self.save_png:
            fig.savefig(self.filename[:-4] + '_' +
                        suffix + '.png', bbox_inches=bbox_inches)
            increase_count = True
        if self.save_single_pdf:
            fig.savefig(self.filename[:-4] + '_' +
                        suffix + '.pdf', bbox_inches=bbox_inches)
            increase_count = True
        if increase_count:
            self.plot_cnt += 1

    def _gauss(self, x, *p):
        amplitude, mu, sigma = p
        return amplitude * np.exp(- (x - mu)**2.0 / (2.0 * sigma**2.0))

    def _lin(self, x, *p):
        m, b = p
        return m * x + b

    def _add_text(self, fig):
        fig.subplots_adjust(top=0.85)
        y_coord = 0.92
        if self.qualitative:
            fig.text(0.1, y_coord, 'RD53A qualitative',
                     fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            if self.run_config['chip_id'] is not None:
                fig.text(0.7, y_coord, 'Chip S/N: 0x0000',
                         fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        else:
            fig.text(0.1, y_coord, 'RD53A %s' %
                     (self.level), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
            if self.run_config['chip_id'] is not None:
                fig.text(0.7, y_coord, 'Chip S/N: %s' %
                         (self.run_config['chip_id']), fontsize=12, color=OVERTEXT_COLOR, transform=fig.transFigure)
        if self.internal:
            fig.text(0.1, 1, 'RD53 Internal', fontsize=16, color='r', rotation=45, bbox=dict(
                boxstyle='round', facecolor='white', edgecolor='red', alpha=0.7), transform=fig.transFigure)

    def _convert_to_e(self, dac):
        return dac * ELECTRON_CONVERSION['slope'] + ELECTRON_CONVERSION['offset']

    def _add_electron_axis(self, fig, ax):
        fig.subplots_adjust(top=0.75)
        ax.title.set_position([.5, 1.15])

        fig.canvas.draw()
        ax2 = ax.twiny()
        xticks = [int(self._convert_to_e(float(x.get_text())))
                  for x in ax.xaxis.get_majorticklabels()]
        ax2.set_xticklabels(xticks)

        l = ax.get_xlim()
        l2 = ax2.get_xlim()

        def f(x): return l2[0] + (x - l[0]) / (l[1] - l[0]) * (l2[1] - l2[0])
        ticks = f(ax.get_xticks())
        ax2.xaxis.set_major_locator(matplotlib.ticker.FixedLocator(ticks))

#         ax2.set_xlabel(r'Electrons ($x \cdot %1.2f \; \frac{e^-}{\Delta VCAL} + %1.2f \; e^-$)' % (ELECTRON_CONVERSION['slope'], ELECTRON_CONVERSION['offset']), labelpad=7)
        ax2.set_xlabel('Electrons', labelpad=7)

    def _plot_parameter_page(self):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        ax.axis('off')

        scan_id = self.run_config['scan_id']
        run_name = self.run_config['run_name']
        chip_id = self.run_config['chip_id']
        sw_ver = self.run_config['software_version']

        if self.level is not '':
            text = 'This is a bdaq53 %s for chip %s.\nRun name: %s' % (
                scan_id, chip_id, run_name)
        else:
            text = 'This is a bdaq53 %s for chip %s.\nRun name: %s' % (
                scan_id, chip_id, run_name)
        ax.text(0.01, 1, text, fontsize=10)
        ax.text(0.9, -0.11, 'Software version: %s' % (sw_ver), fontsize=3)

        if scan_id in ['threshold_scan']:
            ax.text(0.01, 0.02, r'Charge calibration: $y \; [e^-] = x \; [\Delta VCAL] \cdot %1.2f \; \frac{e^-}{\Delta VCAL} + %1.0f \; e^-$' % (
                ELECTRON_CONVERSION['slope'], ELECTRON_CONVERSION['offset']), fontsize=6)

        if 'maskfile' in self.run_config.keys() and self.run_config['maskfile'] is not None and not self.run_config['maskfile'] == 'None':
            ax.text(0.01, -0.05, 'Maskfile:\n%s' %
                    (self.run_config['maskfile']), fontsize=6)

        tb_dict = OrderedDict(sorted(self.dacs.items()))
        for key, value in self.run_config.iteritems():
            if key in ['scan_id', 'run_name', 'chip_id', 'software_version', 'disable', 'maskfile']:
                continue
            tb_dict[key] = value

        tb_list = []
        for i in range(0, len(tb_dict.keys()), 3):
            try:
                key1 = tb_dict.keys()[i]
                value1 = tb_dict[key1]
                try:
                    key2 = tb_dict.keys()[i + 1]
                    value2 = tb_dict[key2]
                except:
                    key2 = ''
                    value2 = ''
                try:
                    key3 = tb_dict.keys()[i + 2]
                    value3 = tb_dict[key3]
                except:
                    key3 = ''
                    value3 = ''
                tb_list.append(
                    [key1, value1, '', key2, value2, '', key3, value3])
            except:
                pass

        widths = [0.2, 0.12, 0.1, 0.2, 0.12, 0.1, 0.2, 0.12]
        labels = ['Parameter', 'Value', '', 'Parameter',
                  'Value', '', 'Parameter', 'Value']
        table = ax.table(cellText=tb_list, colWidths=widths,
                         colLabels=labels, cellLoc='left', loc='center')
        table.scale(0.8, 0.8)

        for key, cell in table.get_celld().items():
            row, col = key
            if row == 0:
                cell.set_color('#ffb300')
                cell.set_fontsize(8)
            if col in [2, 5]:
                cell.set_color('white')
                cell.set_fontsize(8)
            if col in [1, 4, 7]:
                cell._loc = 'center'

        self._save_plots(fig, suffix='parameter_page')

    def _plot_1d_hist(self, hist, yerr=None, title=None, x_axis_title=None, y_axis_title=None, x_ticks=None, color='r', plot_range=None, log_y=False, suffix=None):
        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        hist = np.array(hist)
        if plot_range is None:
            plot_range = range(0, len(hist))
        plot_range = np.array(plot_range)
        plot_range = plot_range[plot_range < len(hist)]
        if yerr is not None:
            ax.bar(x=plot_range, height=hist[plot_range],
                   color=color, align='center', yerr=yerr)
        else:
            ax.bar(x=plot_range,
                   height=hist[plot_range], color=color, align='center')
        ax.set_xlim((min(plot_range) - 0.5, max(plot_range) + 0.5))

        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        if x_ticks is not None:
            ax.set_xticks(plot_range)
            ax.set_xticklabels(x_ticks)
            ax.tick_params(which='both', labelsize=8)
        if np.allclose(hist, 0.0):
            ax.set_ylim((0, 1))
        else:
            if log_y:
                ax.set_yscale('log')
                ax.set_ylim((1e-1, np.amax(hist) * 2))
        ax.grid(True)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_tot(self, hist, title=None):
        if title is None:
            if self.qualitative:
                title = 'Time-over-Threshold distribution'
            else:
                title = ('Time-over-Threshold distribution' +
                         r' ($\Sigma$ = %d)' % (np.sum(hist)))
        self._plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=range(
            0, 16), x_axis_title='ToT code', y_axis_title='# of hits', color='b', suffix='tot')

    def _plot_relative_bcid(self, hist, title=None):
        if title is None:
            if self.qualitative:
                title = 'Relative BCID'
            else:
                title = ('Relative BCID' + r' ($\Sigma$ = %d)' %
                         (np.sum(hist)))
        self._plot_1d_hist(hist=hist, title=title, log_y=True, plot_range=range(
            0, 32), x_axis_title='Relative BCID [25 ns]', y_axis_title='# of hits', suffix='rel_bcid')

    def _plot_event_status(self, hist, title=None):
        self._plot_1d_hist(hist=hist,
                           title=('Event status' + r' ($\Sigma$ = %d)' %
                                  (np.sum(hist))) if title is None else title,
                           log_y=True,
                           plot_range=range(0, 8),
                           x_ticks=('User K\noccured', 'Ext\ntrigger', 'TDC\nword', 'BCID\nerror', 'TRG ID\nerror',
                                    'TDC\nambig.', 'Truncated', 'Unknown\nword'),
                           color='g', y_axis_title='Number of events', suffix='event_status')

    def _plot_bcid_error(self, hist, title=None):
        self._plot_1d_hist(hist=hist,
                           title=('BCID error' + r' ($\Sigma$ = %d)' %
                                  (np.sum(hist))) if title is None else title,
                           log_y=True,
                           plot_range=range(0, 32),
                           x_axis_title='Trigger ID',
                           y_axis_title='Number of event header', color='g',
                           suffix='bcid_error')

    def _plot_cl_size(self, hist):
        ''' Create 1D cluster size plot w/wo log y-scale '''
        self._plot_1d_hist(hist=hist, title='Cluster size',
                           log_y=False, plot_range=range(0, 10),
                           x_axis_title='Cluster size',
                           y_axis_title='# of hits', suffix='cluster_size')
        self._plot_1d_hist(hist=hist, title='Cluster size (log)',
                           log_y=True, plot_range=range(0, 100),
                           x_axis_title='Cluster size',
                           y_axis_title='# of hits', suffix='cluster_size_log')

    def _plot_cl_tot(self, hist):
        ''' Create 1D cluster size plot w/wo log y-scale '''
        self._plot_1d_hist(hist=hist, title='Cluster ToT',
                           log_y=False, plot_range=range(0, 96),
                           x_axis_title='Cluster ToT [25 ns]',
                           y_axis_title='# of hits', suffix='cluster_tot')

    def _plot_cl_shape(self, hist):
        ''' Create a histogram with selected cluster shapes '''
        x = np.arange(12)
        fig = Figure()
        _ = FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        selected_clusters = hist[[1, 3, 5, 6, 9, 13, 14, 7, 11, 19, 261, 15]]
        ax.bar(x, selected_clusters, align='center')
        ax.xaxis.set_ticks(x)
        fig.subplots_adjust(bottom=0.2)
        ax.set_xticklabels([u"\u2004\u2596",
                            # 2 hit cluster, horizontal
                            u"\u2597\u2009\u2596",
                            # 2 hit cluster, vertical
                            u"\u2004\u2596\n\u2004\u2598",
                            u"\u259e",  # 2 hit cluster
                            u"\u259a",  # 2 hit cluster
                            u"\u2599",  # 3 hit cluster, L
                            u"\u259f",  # 3 hit cluster
                            u"\u259b",  # 3 hit cluster
                            u"\u259c",  # 3 hit cluster
                            # 3 hit cluster, horizontal
                            u"\u2004\u2596\u2596\u2596",
                            # 3 hit cluster, vertical
                            u"\u2004\u2596\n\u2004\u2596\n\u2004\u2596",
                            # 4 hit cluster
                            u"\u2597\u2009\u2596\n\u259d\u2009\u2598"])
        ax.set_title('Cluster shapes', color=TITLE_COLOR)
        ax.set_xlabel('Cluster shape')
        ax.set_ylabel('# of hits')
        ax.grid(True)
        ax.set_yscale('log')
        ax.set_ylim(ymin=1e-1)

        if self.qualitative:
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix='cluster_shape')

    def _plot_occupancy(self, hist, electron_axis=False, title='Occupancy', z_label='# of hits', z_min=None, z_max=None, show_sum=True, suffix=None):
        if z_max == 'median':
            z_max = 2 * np.ma.median(hist)
        elif z_max == 'maximum' or z_max is None:
            z_max = np.ma.max(hist)
        if z_max < 1 or hist.all() is np.ma.masked:
            z_max = 1.0

        if z_min is None:
            z_min = np.ma.min(hist)
        if z_min == z_max or hist.all() is np.ma.masked:
            z_min = 0

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.set_adjustable('box')
        extent = [0.5, 400.5, 192.5, 0.5]
        bounds = np.linspace(start=z_min, stop=z_max, num=255, endpoint=True)
        cmap = cm.get_cmap('plasma')
        cmap.set_bad('w', 1.0)
        norm = colors.BoundaryNorm(bounds, cmap.N)

        im = ax.imshow(hist, interpolation='none', aspect='equal', cmap=cmap,
                       norm=norm, extent=extent)  # TODO: use pcolor or pcolormesh
        ax.set_ylim((192.5, 0.5))
        ax.set_xlim((0.5, 400.5))
        if self.qualitative or not show_sum:
            ax.set_title(title, color=TITLE_COLOR)
        else:
            ax.set_title(title + r' ($\Sigma$ = {0})'.format(
                (0 if hist.all() is np.ma.masked else np.ma.sum(hist))), color=TITLE_COLOR)
        ax.set_xlabel('Column')
        ax.set_ylabel('Row')

        divider = make_axes_locatable(ax)
        if electron_axis:
            pad = 1.0
        else:
            pad = 0.6
        cax = divider.append_axes("bottom", size="5%", pad=pad)
        cb = fig.colorbar(im, cax=cax, ticks=np.linspace(
            start=z_min, stop=z_max, num=10, endpoint=True), orientation='horizontal')
        cax.set_xticklabels([int(round(float(x.get_text())))
                             for x in cax.xaxis.get_majorticklabels()])
        cb.set_label(z_label)

        if electron_axis:
            fig.canvas.draw()
            ax2 = cb.ax.twiny()

            pos = ax2.get_position()
            pos.y1 = 0.14
            ax2.set_position(pos)

            for spine in ax2.spines.values():
                spine.set_visible(False)

            xticks = [int(round(self._convert_to_e(float(x.get_text()))))
                      for x in cax.xaxis.get_majorticklabels()]
            ax2.set_xticklabels(xticks)
#
            l = cax.get_xlim()
            l2 = ax2.get_xlim()

            def f(x): return l2[0] + (x - l[0]) / \
                (l[1] - l[0]) * (l2[1] - l2[0])
            ticks = f(cax.get_xticks())
            ax2.xaxis.set_major_locator(matplotlib.ticker.FixedLocator(ticks))

#             ax2.set_xlabel(r'%s [Electrons ($x \cdot %1.2f \; \frac{e^-}{\Delta VCAL} + %1.2f \; e^-$)]' % (z_label, ELECTRON_CONVERSION['slope'], ELECTRON_CONVERSION['offset']), labelpad=7)
            ax2.set_xlabel('%s [Electrons]' % (z_label), labelpad=7)
            cb.set_label(r'%s [$\Delta$ VCAL]' % z_label)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
            cb.formatter = plt.NullFormatter()
            cb.update_ticks()

        self._save_plots(fig, suffix=suffix)

    def _plot_scurves(self, scurves, scan_parameters, electron_axis=False, scan_parameter_name=None, title='S-curves', ylabel='Occupancy'):
        start_column = int(self.run_config['start_column'])
        stop_column = int(self.run_config['stop_column'])
        start_row = int(self.run_config['start_row'])
        stop_row = int(self.run_config['stop_row'])

        max_occ = np.max(scurves) + 5  # Maybe?
        x_bins = scan_parameters  # np.arange(-0.5, max(scan_parameters) + 1.5)
        y_bins = np.arange(-0.5, max_occ + 0.5)
        n_pixel = (stop_column - start_column) * (stop_row - start_row)

        param_count = scurves.shape[0]
        hist = np.empty([param_count, max_occ], dtype=np.uint32)

        for param in range(param_count):
            hist[param] = np.bincount(scurves[param, start_column * 192 + start_row:(
                stop_column - 1) * 192 + stop_row], minlength=max_occ)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        fig.patch.set_facecolor('white')
        cmap = cm.get_cmap('cool')
        if np.allclose(hist, 0.0) or hist.max() <= 1:
            z_max = 1.0
        else:
            z_max = hist.max()
        # for small z use linear scale, otherwise log scale
        if z_max <= 10.0:
            bounds = np.linspace(start=0.0, stop=z_max, num=255, endpoint=True)
            norm = colors.BoundaryNorm(bounds, cmap.N)
        else:
            bounds = np.linspace(start=1.0, stop=z_max, num=255, endpoint=True)
            norm = colors.LogNorm()

        im = ax.pcolormesh(x_bins, y_bins, hist.T, norm=norm, rasterized=True)

        if z_max <= 10.0:
            cb = fig.colorbar(im, ticks=np.linspace(start=0.0, stop=z_max, num=min(
                11, math.ceil(z_max) + 1), endpoint=True), fraction=0.04, pad=0.05)
        else:
            cb = fig.colorbar(im, fraction=0.04, pad=0.05)
        cb.set_label("# of pixels")
        ax.set_title(title + ' for %d pixel(s)' % (n_pixel), color=TITLE_COLOR)
        if scan_parameter_name is None:
            ax.set_xlabel('Scan parameter')
        else:
            ax.set_xlabel(scan_parameter_name)
        ax.set_ylabel(ylabel)

        if electron_axis:
            self._add_electron_axis(fig, ax)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())
            cb.formatter = plt.NullFormatter()
            cb.update_ticks()

        self._save_plots(fig, suffix='scurves')

    def _plot_distribution(self, data, plot_range=None, x_axis_title=None, electron_axis=False, y_axis_title='# of hits', title=None, suffix=None):
        start_column = int(self.run_config['start_column'])
        stop_column = int(self.run_config['stop_column'])

        data = data[:, start_column:stop_column]
        if plot_range is None:
            diff = np.amax(data) - np.amin(data)
            if (np.amax(data)) > np.median(data) * 5:
                plot_range = np.arange(
                    np.amin(data), np.median(data) * 5, diff / 100.)
            else:
                plot_range = np.arange(np.amin(data), np.amax(
                    data) + diff / 100., diff / 100.)

        tick_size = plot_range[1] - plot_range[0]

        hist, bins = np.histogram(np.ravel(data), bins=plot_range)

        bin_centres = (bins[:-1] + bins[1:]) / 2
        p0 = (np.amax(hist), np.mean(bins),
              (max(plot_range) - min(plot_range)) / 3)

        try:
            coeff, _ = curve_fit(self._gauss, bin_centres, hist, p0=p0)
        except:
            coeff = None
            self.logger.warning('Gauss fit failed!')

        if coeff is not None:
            points = np.linspace(min(plot_range), max(plot_range), 500)
            gau = self._gauss(points, *coeff)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.bar(bins[:-1], hist, width=tick_size, align='edge')
        if coeff is not None:
            ax.plot(points, gau, "r-", label='Normal distribution')

        ax.set_xlim((min(plot_range), max(plot_range)))
        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid(True)

        if coeff is not None and not self.qualitative:
            if coeff[1] < 10:
                if electron_axis:
                    textright = '$\mu=%.1f\;\Delta$VCAL\n$\;\,\,=%.0f \; e^-$\n\n$\sigma=%.1f\;\Delta$VCAL\n$\;\,\,=%.0f \; e^-$' % (
                        abs(coeff[1]), self._convert_to_e(abs(coeff[1])), abs(coeff[2]), self._convert_to_e(abs(coeff[2])))
                else:
                    textright = '$\mu=%.1f$\n$\sigma=%.1f$' % (abs(coeff[1]), abs(coeff[2]))
            else:
                if electron_axis:
                    textright = '$\mu=%.0f\;\Delta$VCAL\n$\;\,\,=%.0f \; e^-$\n\n$\sigma=%.0f\;\Delta$VCAL\n$\;\,\,=%.0f \; e^-$' % (
                        abs(coeff[1]), self._convert_to_e(abs(coeff[1])), abs(coeff[2]), self._convert_to_e(abs(coeff[2])))
                else:
                    textright = '$\mu=%.0f$\n$\sigma=%.0f$' % (abs(coeff[1]), abs(coeff[2]))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax.text(0.05, 0.9, textright, transform=ax.transAxes,
                    fontsize=8, verticalalignment='top', bbox=props)

        if electron_axis:
            self._add_electron_axis(fig, ax)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_stacked_threshold(self, data, tdac_mask, plot_range=None, electron_axis=False, x_axis_title=None, y_axis_title=None,
                                title=None, suffix=None, min_tdac=0, max_tdac=15, range_tdac=16):
        start_column = int(self.run_config['start_column'])
        stop_column = int(self.run_config['stop_column'])
        data = data[:, start_column:stop_column]

        if plot_range is None:
            diff = np.amax(data) - np.amin(data)
            if (np.amax(data)) > np.median(data) * 5:
                plot_range = np.arange(
                    np.amin(data), np.median(data) * 5, diff / 100.)
            else:
                plot_range = np.arange(np.amin(data), np.amax(
                    data) + diff / 100., diff / 100.)

        tick_size = plot_range[1] - plot_range[0]

        hist, bins = np.histogram(np.ravel(data), bins=plot_range)

        bin_centres = (bins[:-1] + bins[1:]) / 2
        p0 = (np.amax(hist), np.mean(bins),
              (max(plot_range) - min(plot_range)) / 3)

        try:
            coeff, _ = curve_fit(self._gauss, bin_centres, hist, p0=p0)
        except:
            coeff = None
            self.logger.warning('Gauss fit failed!')

        if coeff is not None:
            points = np.linspace(min(plot_range), max(plot_range), 500)
            gau = self._gauss(points, *coeff)

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        cmap = cm.get_cmap('viridis', (range_tdac - 1))
        # create dicts for tdac data
        data_thres_tdac = {}
        hist_tdac = {}
        tdac_bar = {}

        tdac_map = tdac_mask[:, start_column:stop_column]

        # select threshold data for different tdac values according to tdac map
        for tdac in range(range_tdac):
            data_thres_tdac[tdac] = data[tdac_map == tdac - abs(min_tdac)]
            # histogram threshold data for each tdac
            hist_tdac[tdac], _ = np.histogram(
                np.ravel(data_thres_tdac[tdac]), bins=bins)

            if tdac == 0:
                tdac_bar[tdac] = ax.bar(bins[:-1], hist_tdac[tdac], width=tick_size,
                                        align='edge', color=cmap(.9 / range_tdac * tdac), edgecolor='white')
            elif tdac == 1:
                tdac_bar[tdac] = ax.bar(bins[:-1], hist_tdac[tdac], bottom=hist_tdac[0], width=tick_size,
                                        align='edge', color=cmap(1. / range_tdac * tdac), edgecolor='white')
            else:
                tdac_bar[tdac] = ax.bar(bins[:-1], hist_tdac[tdac], bottom=np.sum([hist_tdac[i] for i in range(
                    tdac)], axis=0), width=tick_size, align='edge', color=cmap(1. / range_tdac * tdac), edgecolor='white')

        fig.subplots_adjust(right=0.85)
        cax = fig.add_axes([0.89, 0.11, 0.02, 0.645])
        sm = plt.cm.ScalarMappable(
            cmap=cmap, norm=colors.Normalize(vmin=min_tdac, vmax=max_tdac))
        sm.set_array([])
        cb = fig.colorbar(sm, cax=cax, ticks=np.linspace(
            start=min_tdac, stop=max_tdac, num=range_tdac, endpoint=True))
        cb.set_label('TDAC')

        if coeff is not None:
            ax.plot(points, gau, "r-", label='Normal distribution')

        ax.set_xlim((min(plot_range), max(plot_range)))
        ax.set_title(title, color=TITLE_COLOR)
        if x_axis_title is not None:
            ax.set_xlabel(x_axis_title)
        if y_axis_title is not None:
            ax.set_ylabel(y_axis_title)
        ax.grid(True)

        if coeff is not None and not self.qualitative:
            if electron_axis:
                textright = '$\mu=%.0f\;\Delta$VCAL\n$\;\,\,=%.0f \; e^-$\n\n$\sigma=%.0f\;\Delta$VCAL\n$\;\,\,=%.0f \; e^-$' % (
                    abs(coeff[1]), self._convert_to_e(abs(coeff[1])), abs(coeff[2]), self._convert_to_e(abs(coeff[2])))
            else:
                textright = '$\mu=%.0f$\n$\sigma=%.0f$' % (abs(coeff[1]), abs(coeff[2]))
            props = dict(boxstyle='round', facecolor='wheat', alpha=0.5)
            ax.text(0.05, 0.9, textright, transform=ax.transAxes,
                    fontsize=8, verticalalignment='top', bbox=props)

        if electron_axis:
            self._add_electron_axis(fig, ax)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_dac_linearity(self, data, title=None, x_axis_title=None, y_axis_title=None, second_axis=True,
                            suffix='linearity', adc_ref=0.8, I_ref=4):
        x, y = [], []
        for element in data:
            x = np.append(x, element[0])
            y = np.append(y, element[1])

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.plot(x, y, 'o', color='#07529a', label='Data')

        try:
            p0 = (1., 0.)
            coeff, _ = curve_fit(self._lin, x, y, p0=p0)
        except:
            coeff = (0, 0)
            self.logger.error('Linear fit failed!')

        m, b = coeff
        ax.plot(x, self._lin(x, *coeff), 'r-', label='Fit')

        if y[0] < y[-3]:
            coords = (max(x), min(y))
            halign = 'right'
        else:
            coords = (0, 0)
            halign = 'left'
        if b >= 0:
            sign = '+'
        else:
            sign = '-'
        if self.run_config['type'] == 'U':
            ax.text(coords[0], coords[1], 'f(x) = %.2f x %s %.2f\nVREF_ADC_IN = %.2f V' % (abs(m), sign, abs(
                b), adc_ref), bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5), horizontalalignment=halign)
        if self.run_config['type'] == 'I':
            ax.text(coords[0], coords[1], 'f(x) = %.2f x %s %.2f\nIref = %.2f $\mu$A' % (abs(m), sign, abs(
                b), I_ref), bbox=dict(boxstyle='round', facecolor='wheat', alpha=0.5), horizontalalignment=halign)

        ax.legend()
        ax.grid()
        ax.set_title(str(title), color=TITLE_COLOR)

        if second_axis:
            ax2 = ax.twinx()
            ax2.plot(x, y * 0.00019, linestyle='None')
            ax2.set_ylabel('ADC [V]')

        if x_axis_title is None or self.qualitative:
            ax.set_xlabel('Register Setting [LSB]')
        else:
            if not '[LSB]' in x_axis_title:
                x_axis_title += ' [LSB]'
            ax.set_xlabel(x_axis_title)

        if y_axis_title is None:
            ax.set_ylabel('Voltmeter [V]')
        else:
            ax.set_ylabel(y_axis_title)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)

    def _plot_dac_non_linearity(self, data, title=None, typ='DNL', x_axis_title=None, y_axis_title=None, suffix='linearity'):
        x, y = [], []
        if typ == 'DNL':
            for element in data:
                x = np.append(x, element[0])
                y = np.append(y, element[2])
            title = 'Differential Non-Linearity'
        if typ == 'INL':
            for element in data:
                x = np.append(x, element[0])
                y = np.append(y, element[3])
            title = 'Integrated Non-Linearity'

        fig = Figure()
        FigureCanvas(fig)
        ax = fig.add_subplot(111)
        self._add_text(fig)

        ax.plot(x, y, '.', color='#07529a', markersize=2,
                linewidth=0.5, label='Data')

        ax.grid()
        ax.set_title(title, color=TITLE_COLOR)

        if x_axis_title is None:
            ax.set_xlabel('Register Setting [LSB]')
        else:
            if not '[LSB]' in x_axis_title:
                x_axis_title += ' [LSB]'
            ax.set_xlabel(x_axis_title)

        if y_axis_title is None:
            ax.set_ylabel('Voltmeter [V]')
        else:
            ax.set_ylabel(y_axis_title)

        if self.qualitative:
            ax.xaxis.set_major_formatter(plt.NullFormatter())
            ax.xaxis.set_minor_formatter(plt.NullFormatter())
            ax.yaxis.set_major_formatter(plt.NullFormatter())
            ax.yaxis.set_minor_formatter(plt.NullFormatter())

        self._save_plots(fig, suffix=suffix)


if __name__ == "__main__":
    ''' Path to analyzed data file: '''
    analyzed_data_file = '_interpreted.h5'

    with Plotting(analyzed_data_file=analyzed_data_file,
                  pdf_file=None,            # Path to output pdf file. If None, the name of the analyzed data file is used
                  level='',                 # Level of the results. For example 'preliminary'
                  qualitative=False,        # Create qualitative plots without numbers on axes
                  internal=False,           # Write 'RD53 internal' on every plot
                  save_single_pdf=False,    # Save every plot as a single pdf file in addition to the one output pdf file
                  save_png=False) as p:     # Save every plot as a single png file in addition to the one output pdf file

        ''' Select which plots to create by uncommenting '''
        p.create_standard_plots()           # Creates all the standard plots like a BDAQ53 scan would
        ''' Either create all standard plots using the line above or select each plot independently below '''
#         p.create_parameter_page()           # Creates a title page containing a table with all used parameters and DAC settings
#         p.create_event_status_plot()        # Creates an event status plot showing a histogram of all errors that occurred during analysis
#         p.create_occupancy_map()            # Creates an occupancy hitmap of the chip
#         p.create_tot_plot()                 # Creates a histogram of the occurred ToT values
#         p.create_rel_bcid_plot()            # Creates a histogram of the occurred relative BCIDs
#         p.create_tdac_plot()                # Creates a histogram of the TDAC distribution

        ''' The following plots can only be created from threshold scan or tuning script data '''
#         p.create_scurves_plot()             # Create a 2D-histogram of the S-Curves of all pixels
#         p.create_threshold_plot()           # Creates a threshold distribution plot including a gaussian fit function
#         p.create_stacked_threshold_plot()   # Creates a threshold distribution plot where each bar shows the TDAC distribution in this bin
#         p.create_threshold_map()            # Creates a hitmap showing the threshold value of each pixel
#         p.create_noise_plot()               # Creates a noise distribution plot including a gaussian fit function
#         p.create_noise_map()                # Creates a hitmap showing the noise value of each pixel
#         p.create_chi2_map()                 # Creates a hitmap showing the chi squared values from the S-Curve fit of each pixel

        ''' The following plots can only be created from clusterized data '''
#         p.create_cluster_tot_plot()         # Creates a histogram of the clustered ToT values
#         p.create_cluster_shape_plot()       # Creates a histogram of the occurred cluster shapes
#         p.create_cluster_size_plot()        # Creates a histogram of the occurred cluster sizes
