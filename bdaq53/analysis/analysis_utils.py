#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

from __future__ import print_function
from collections import OrderedDict
import ast
import logging
import multiprocessing as mp
from functools import partial
from scipy.special import erf

from scipy.optimize import curve_fit
import numba
import numpy as np
from tqdm import tqdm

from bdaq53.register_utils import RD53ARegisterParser

logger = logging.getLogger('Analysis')
rp = RD53ARegisterParser()

# Word defines
USERK_FRAME_ID = 0x01000000
HEADER_ID = 0x00010000
TRIGGER_ID = 0x80000000

# Data Masks
BCID_MASK = 0x7fff
TRG_MASK = 0x7FFFFFFF  # Trigger data (number and/or time stamp)

# Event status bits
E_USER_K = 0x00000001  # event has user K words
E_EXT_TRG = 0x00000002  # event has trigger word from RO system
E_TDC = 0x00000004  # event has TDC word(s) from RO system
E_BCID_INC_ERROR = 0x00000008  # BCID does not increase as expected
E_TRG_ID_INC_ERROR = 0x00000010  # TRG ID does not increase by 1
E_TDC_AMBIGUOUS = 0x00000020  # unique TDC hit assignment impossible
E_EVENT_TRUNC = 0x00000040  # event data interpretation aborted
E_UNKNOWN_WORD = 0x00000080  # unknown word occured
# Event structure wrong (hit before header or number of data header wrong)
E_STRUCT_WRONG = 0x00000100
E_EXT_TRG_ERR = 0x00000200  # event has external trigger number increase error


class ConfigDict(dict):
    ''' Dictionary with different key data types:
        str / int / float depending on value
    '''

    def __init__(self, *args):
        super(ConfigDict, self).__init__(*args)

    def __getitem__(self, key):
        val = dict.__getitem__(self, key)
        if 'chip_id' in key:
            return val
        try:
            return ast.literal_eval(val)
        except (ValueError, SyntaxError):  # fallback to return a string
            return val


def scurve(x, A, mu, sigma):
    return 0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


def zcurve(x, A, mu, sigma):
    return -0.5 * A * erf((x - mu) / (np.sqrt(2) * sigma)) + 0.5 * A


@numba.njit
def translate_mapping(core_column, region, pixel_id):
    '''
        Translate mapping between raw data format (core column and region) and
        absolute column and row.

        ----------
        Parameters:
            core_column : int
                Core column number [0:49]
            region : int
                Region in the core column [0:383]
            pixel_id : int
                Pixel in the region [0:3]

        Returns:
            column : int
                Absolute column number [0:399]
            row : int
                Absolute row number [0:191]
    '''

    if pixel_id > 3:
        raise ValueError('pixel_id cannot be larger than 3!')
    column = core_column * 8 + pixel_id
    if region % 2 == 1:
        column += 4
    row = int(region / 2)

    return column, row


@numba.njit
def build_event(hits, data_out_i, hist_occ, hist_tot, hist_rel_bcid,
                hist_scurve, hist_event_status, hit_buffer, hit_buffer_i,
                start_bcid, scan_param_id, event_status):
    '''
        Fill result data structures (hit array and histograms) from hit buffer.

        To be called at the end of one event.
    '''

    # Copy hits from buffer to result data structure
    for i in range(hit_buffer_i):
        hits[data_out_i] = hit_buffer[i]
        # Set relative BCID of event hits
        if start_bcid < hit_buffer[i]['bcid']:
            hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] - start_bcid
        else:  # Overflow of 15-bit bcid
            hits[data_out_i]['rel_bcid'] = hit_buffer[i]['bcid'] + 2**15 - start_bcid

        # Only set relative BCID of events where event building worked
        if (not (event_status & E_BCID_INC_ERROR) and
            not (event_status & E_TRG_ID_INC_ERROR) and
                not (event_status & E_STRUCT_WRONG)):
            if hits[data_out_i]['rel_bcid'] < 32:
                hist_rel_bcid[hits[data_out_i]['rel_bcid']] += 1

        hits[data_out_i]['scan_param_id'] = scan_param_id
        hits[data_out_i]['event_status'] = event_status
        # Fill histograms
        hist_occ[hits[data_out_i]['col'], hits[data_out_i]['row']] += 1
        hist_tot[hits[data_out_i]['tot']] += 1
        hist_scurve[hits[data_out_i]['col'] * 192 +
                    hits[data_out_i]['row'], scan_param_id] += 1

        data_out_i += 1

    for i in range(32):
        if event_status & (0b1 << i):
            hist_event_status[i] += 1

    return data_out_i


@numba.njit
def add_hits(data_word, hit_buffer, hit_buffer_i, event_number, trg_id, bcid,
             trg_tag, event_status):
    multicol = (data_word >> 26) & 0x3f
    region = (data_word >> 16) & 0x3ff

    for i in range(4):
        col, row = translate_mapping(multicol, region, i)
        tot = (data_word >> i * 4) & 0xf

        if col < 400 and row < 192:
            if tot != 255 and tot != 15:
                hit_buffer[hit_buffer_i]['bcid'] = bcid
                hit_buffer[hit_buffer_i]['event_number'] = event_number
                hit_buffer[hit_buffer_i]['trigger_id'] = trg_id
                hit_buffer[hit_buffer_i]['col'] = col
                hit_buffer[hit_buffer_i]['row'] = row
                hit_buffer[hit_buffer_i]['tot'] = tot
                hit_buffer[hit_buffer_i]['trigger_tag'] = trg_tag
                hit_buffer_i += 1
        else:
            event_status |= E_UNKNOWN_WORD

    return hit_buffer_i, event_status


@numba.njit
def number_of_set_bits(i):
    ''' Count set bits of 32-bit integer

    Variable-precision SWAR algorithm
    '''
    i = i - ((i >> 1) & 0x55555555)
    i = (i & 0x33333333) + ((i >> 2) & 0x33333333)
    return (((i + (i >> 4) & 0xF0F0F0F) * 0x1010101) & 0xffffffff) >> 24


@numba.njit
def is_new_event(n_event_header, n_trigger, start_trig_id, trig_id,
                 start_bcid, bcid, prev_bcid, last_bcid, trg_header,
                 event_status, is_bcid_offset, method):
    ''' Detect new event by different methods.

        Note:
        -----
        Checks for new events are rather complex to handle all possible
        cases. They are tested with high coverage in unit tests, do not
        make quick chances without checking that event building still works!

        Methods to do event alignment, create new event if:
        0: - Number of event header (n_event_header) exceeds the
             number of sub-triggers used during data taking (n_trigger)
             AND
             - BCID offset flag is not set
               (happens if previous event has too many event headers)
               AND
               - Event has BCID increase error
                 (increases likelihood that event has previous event headers)
               AND
               - The actual BCID follows the last BCID
                 (increases likelihood that actual event has an additional BCID)
          OR
           - Trigger ID does not meet expectation: trigger_id = start_trig_id + n_event_header
             AND
             - The BCID is also different from expectation: bcid = prev_bcid + 1
             AND NOT
             - Both counters are off with different offset. Same offset
               is expected if event header(s) is not recognized
          OR
           - The event is flagged to have a not trust worthy trigger ID.
             (this is the case if the trigger ID jumps within the event or the
              predecessing event had a wrong structure)
             AND
             - The BCID is different from expectation bcid = start_bcid + n_event_header
               (this is effectively a fallback to BCID alignment if trigger ID
               is already not trust worthy)
        1: Data word is TLU trigger word. This is expected to be at the beginning of an event.
    '''

    if method == 0:
        # Number of trigge in event exceeded
        if n_event_header >= n_trigger:
            # Check if previous events data header are likely in this event
            if not is_bcid_offset:
                return True
            else:
                # Event with header of old event must have BCID error and
                if (event_status & E_BCID_INC_ERROR and
                        check_difference(last_bcid, bcid, bits=15)):
                    return False
                else:
                    return True

        # Trigger ID is wrong
        elif not check_difference(start_trig_id + n_event_header, value_2=trig_id, bits=5, delta=0):
            # BCID is also wrong
            if not check_difference(prev_bcid, bcid, bits=15, delta=1):
                # BCID is wrong by the same bunch offset --> assume missed
                # event header continue with event
                if trig_id - start_trig_id == bcid - prev_bcid:
                    return False
                else:  # BCID and trigger ID are wrong, without same relation, likely new event
                    return True
            else:  # BCID is ok, continue with event
                return False
        # Event trigger ID cannot be trusted
        elif (event_status & E_TRG_ID_INC_ERROR):
            # Use BCID to detect new event
            if not check_difference(prev_bcid, bcid, bits=15, delta=1):
                return True
            else:
                return False
        else:
            return False
    elif method == 1:
        return trg_header
    else:
        raise


@numba.njit
def check_difference(value_1, value_2, bits, delta=1):
    ''' Returns true if value_2 - value_1 == delta

        Treads overflow at of value_2 at 2**bits correctly
    '''
    if value_2 >= value_1:
        if value_2 != value_1 + delta:
            return False
        else:
            return True
    else:
        if value_2 + 2**bits != value_1 + delta:
            return False
        else:
            return True


def analyze_chunk(rawdata, return_hists=('HistOcc', ), return_hits=False,
                  scan_param_id=0,
                  trig_pattern=0b11111111111111111111111111111111,
                  align_method=0):
    ''' Helper function to quickly analyze a data chunk.

        Warning
        -------
            If the rawdata contains incomplete event data only data that do
            not need event building are correct (occupancy + tot histograms)

        Parameters
        ----------
        rawdata : np.array, 32-bit dtype
            The raw data containing FE, trigger and TDC words
        return_hists : iterable of strings
            Names to select the histograms to return. Is not case sensitive
            and string must contain only e.g.: occ, tot, bcid, event
        return_hits : boolean
            Return the hit array
        scan_param_id : integer
            Set scan par id in hit info table
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number if event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word

        Usefull for tuning. Analysis per scan parameter not possible.
        Chunks should not be too large.

        Returns
        -------
            ordered dict: With key = return_hists string, value = data
            and first entry are hits when selected
    '''

    n_hits = rawdata.shape[0] * 4
    (hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, hist_event_status,
     hist_bcid_error) = init_outs(n_hits, n_scan_params=1)

    interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_scurve, hist_event_status,
                   hist_bcid_error,
                   scan_param_id=scan_param_id, event_number=0,
                   trig_pattern=trig_pattern,
                   align_method=align_method,
                   prev_trig_id=-1, last_chunk=True)

    hists = {'occ': hist_occ,
             'tot': hist_tot,
             'rel': hist_rel_bcid,
             'curve': hist_scurve,
             'event': hist_event_status,
             'error': hist_bcid_error
             }

    ret = OrderedDict()

    if return_hits:
        ret['hits'] = hits

    for key, value in hists.iteritems():
        for word in return_hists:
            if key in word.lower():
                ret[word] = value
                break

    return ret


@numba.njit
def interpret_data(rawdata, hits, hist_occ, hist_tot,
                   hist_rel_bcid, hist_scurve, hist_event_status,
                   hist_bcid_error,
                   scan_param_id, event_number=0,
                   trig_pattern=0b11111111111111111111111111111111,
                   align_method=0,
                   prev_trig_id=-1, prev_trg_number=-1, last_chunk=False):
    ''' Interprets raw data words to create hits and fill histograms.
        The FE 32-bit data is splitted into two 32-bit words with the
        FE data in the low word:
            1. 32-bit raw data:    16-bit + 16-bit FE high word
            2. 32-bit raw data:    16-bit + 16-bit FE low word

        One event is build for one external trigger! Triggers issued by
        RD53A are sub-triggers and data of these is combined to one event.

        Parameters
        ----------
        rawdata : numpy.array
            Raw data words of actual chunk
        event_number : integer
            The actual event number
        hits : numpy.recarray
            Hit array to be filled.
        hist_occ, hist_tot, hist_rel_bcid, hist_scurve : np.array
            Histograms to be filled.
        scan_param_id : integer
            Actual scan parameter id. Added as hit info.
        trig_pattern : integer (32-bits)
            Indicate the position of the sub-triggers. Needed to check
            the BCIDs.
        align_method : integer
            Methods to do event alignment
            0: New event when number of event headers exceeds number of
               sub-triggers. Many fallbacks for corrupt data implemented.
            1: New event when data word is TLU trigger word
        prev_bcid : integer
            BCID of last chunk. If -1 bcid of first header is used
        prev_trig_id : integer
            Trigger ID of last chunk. If -1 trigger ID of first header is used
        prev_trg_number : integer
            External trigger number of last chunk. If -1 trigger number of
            first trigger word is used
        last_chunk : boolean
            The chunk is the last chunk. Needed to build last event
    '''

    fe_high_word = True
    data_header = False
    trg_header = False
    data_out_i = 0
    bcid = -1
    # Is set if a BCID offset is likely (e.g. too many event header in event)
    is_bcid_offset = False

    # Per call variables
    n_trigger = number_of_set_bits(trig_pattern)
    # Calculate effective not byte aligned pattern (= ommit leading zeros)
    eff_trig_patt = trig_pattern
    for _ in range(32):
        if eff_trig_patt & 0x80000000:
            break
        eff_trig_patt = eff_trig_patt << 1

    # Hit buffer to store actual event hits, needed to set parameters
    # calculated at the end of an event and not available before
    hit_buffer_i = 0  # Index of entry of hit buffer
    hit_buffer = np.zeros_like(hits)
    last_word_index = 0  # Last raw data index of full event

    # Per event variables
    trig_id = -1
    start_bcid = -1
    start_trig_id = -1
    event_status = 0
    n_event_header = 0
    prev_bcid = -1  # BCID before the expected BCID
    last_bcid = -1  # BCID of last event header
    i_pattern = 0  # index in trigger pattern
    trg_number = 0

    for i, word in enumerate(rawdata):
        if word & USERK_FRAME_ID:  # skip USER_K frame
            event_status |= E_USER_K
            continue

        if word & TRIGGER_ID:
            trg_number = word & TRG_MASK
            event_status |= E_EXT_TRG
            trg_header = True
            # Special case for first trigger word
            if align_method == 1 and prev_trg_number == -1:
                prev_trg_number = trg_number
            continue

        if word & HEADER_ID:  # data header
            data_header = True
            fe_high_word = True

        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if fe_high_word:
            data_word = word & 0xffff
            fe_high_word = False  # Next is low word
            continue  # Low word still missing
        else:
            data_word = data_word << 16 | word & 0xffff
            fe_high_word = True  # Next is high word

        if data_header:
            data_header = False

            bcid = data_word & BCID_MASK
            trig_id = (data_word >> 20) & 0x1f
            trig_tag = (data_word >> 15) & 0x1f

            # Special cases for first event/event header
            if start_bcid == -1:
                start_bcid = bcid
            if start_trig_id == -1:
                start_trig_id = trig_id
                if align_method == 0:
                    prev_trg_number = trg_number

            if is_new_event(n_event_header, n_trigger, start_trig_id, trig_id,
                            start_bcid, bcid, prev_bcid, last_bcid, trg_header,
                            event_status, is_bcid_offset, method=align_method):
                # Set event errors
                if n_event_header != n_trigger:
                    event_status |= E_STRUCT_WRONG
                if bcid < 0:
                    event_status |= E_STRUCT_WRONG
                if trg_header and not check_difference(prev_trg_number,
                                                       trg_number, 31):
                    event_status |= E_EXT_TRG_ERR
                # Handle case of too many event header than expected by setting
                # flag to check BCIDs in next event more carfully
                if n_event_header < 32 and check_difference(last_bcid, bcid, 5):
                    is_bcid_offset = True
                else:
                    is_bcid_offset = False

                data_out_i = build_event(hits, data_out_i,
                                         hist_occ, hist_tot, hist_rel_bcid,
                                         hist_scurve, hist_event_status,
                                         hit_buffer, hit_buffer_i, start_bcid,
                                         scan_param_id, event_status)

                event_number += 1
                last_word_index = i - 1  # to get high word

                # Reset per event variables
                hit_buffer_i = 0
                start_bcid = bcid
                start_trig_id = trig_id
                i_pattern = 0
                prev_trg_number = trg_number
                trg_header = False

                # If old event struture is wrong this event is likely wrong too
                # Assume trigger ID error if no external trigger is available
                if (event_status & E_STRUCT_WRONG and align_method == 0):
                    event_status = E_TRG_ID_INC_ERROR
                else:
                    event_status = 0
                n_event_header = 0
            else:  # data word is event header, but not the first of one event
                # Check sanity of BCID counter
                if prev_bcid != -1 and not check_difference(prev_bcid, bcid, 15):
                    # If event has already BCID error it is not sufficient to
                    # check difference to previous BCID. Check to first BCID
                    if event_status & E_BCID_INC_ERROR:
                        if not check_difference(start_bcid + n_event_header, bcid, 15, 0):
                            if 0 < n_event_header <= 32:
                                hist_bcid_error[n_event_header] += 1
                            else:
                                raise
                    else:
                        event_status |= E_BCID_INC_ERROR
                        if 0 < n_event_header <= 32:
                            hist_bcid_error[n_event_header] += 1
                # Check sanity of trigger ID counter
                if prev_trig_id != -1 and not check_difference(prev_trig_id, trig_id, 5):
                    event_status |= E_TRG_ID_INC_ERROR

            # Set event header counter variables
            n_event_header += 1
            prev_trig_id = trig_id
            last_bcid = bcid
            prev_bcid = bcid
            # Correct expected previous BCID by trigger pattern
            for i_pattern in range(i_pattern + 1, 32):
                # Search for next trigger (=1) in trigger pattern
                if eff_trig_patt & (0b1 << (31 - i_pattern)):
                    break
                # Special case: all other bit are all zero
                if ((eff_trig_patt << (i_pattern + 1)) & 0xFFFFFFFF) == 0:
                    break
                else:  # Increase expectation for a 0 in trigger pattern
                    prev_bcid += 1

        else:  # data word is hit data
            if trig_id < 0:
                event_status |= E_STRUCT_WRONG
            hit_buffer_i, event_status = add_hits(data_word, hit_buffer, hit_buffer_i,
                                                  event_number, trig_id, bcid, trig_tag,
                                                  event_status)
    if last_chunk:
        data_out_i = build_event(hits, data_out_i,
                                 hist_occ, hist_tot, hist_rel_bcid,
                                 hist_scurve, hist_event_status, hit_buffer,
                                 hit_buffer_i, start_bcid,
                                 scan_param_id, event_status)
        event_number += 1
        last_word_index = i + 1

    return data_out_i, event_number, last_word_index - rawdata.shape[0], start_trig_id - 1


def interpret_userk_data(rawdata):
    userk_data = np.zeros(shape=rawdata.shape[0],
                          dtype={'names': ['AuroraKWord', 'Status', 'Data1', 'Data1_AddrFlag', 'Data1_Addr', 'Data1_Data', 'Data0', 'Data0_AddrFlag', 'Data0_Addr', 'Data0_Data'],
                                 'formats': ['uint8', 'uint8', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16', 'uint16']})
    userk_word_cnt = 0
    userk_block_cnt = 0
    userk_framelength = 2
    block_temp = 0
    userk_data_i = 0

    for word in rawdata:
        if (word & USERK_FRAME_ID):
            if userk_word_cnt == 0:
                userk_word = word & 0xffff
            else:
                userk_word = userk_word << 16 | word & 0xffff

            if userk_block_cnt == 2 * userk_framelength - 1:
                block_temp = (userk_word & 0x3) << 32 | block_temp
                userk_block = userk_word >> 2
                Data1 = userk_block & 0x7ffffff
                Data0 = (block_temp >> 8) & 0x7ffffff
                userk_data[userk_data_i]['AuroraKWord'] = block_temp & 0xff
                userk_data[userk_data_i]['Status'] = (userk_block >> 30) & 0xf
                userk_data[userk_data_i]['Data1'] = Data1
                userk_data[userk_data_i]['Data1_AddrFlag'] = (
                    Data1 >> 25) & 0x1
                userk_data[userk_data_i]['Data1_Addr'] = (Data1 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data1_Data'] = (Data1 >> 0) & 0xffff
                userk_data[userk_data_i]['Data0'] = Data0
                userk_data[userk_data_i]['Data0_AddrFlag'] = (
                    Data0 >> 25) & 0x1
                userk_data[userk_data_i]['Data0_Addr'] = (Data0 >> 16) & 0x1ff
                userk_data[userk_data_i]['Data0_Data'] = (Data0 >> 0) & 0xffff
                userk_data_i += 1

                userk_block_cnt = 0

            else:
                userk_block_cnt += 1

            # interpret received packet as user k
            if userk_word_cnt >= userk_framelength - 1:
                userk_word_cnt = 0
                block_temp = userk_word
            else:
                userk_word_cnt += 1

    return userk_data[:userk_data_i]


def process_userk(userk_in):
    userk_out = np.zeros(userk_in.shape[0] * 2, dtype={'names': ['Address', 'Name', 'Data'],
                                                       'formats': ['uint16', 'S30', 'uint16']})
    userk_counter = 0
    for i in userk_in:
        AuroraKWord = i['AuroraKWord']
        if AuroraKWord == 0:
            userk_out[userk_counter]['Address'] = i['Data1_Addr']
            userk_out[userk_counter]['Name'] = str(
                rp.get_name(hex(i['Data1_Addr'])))
            userk_out[userk_counter]['Data'] = i['Data1']

            userk_out[userk_counter + 1]['Address'] = i['Data0_Addr']
            userk_out[
                userk_counter + 1]['Name'] = str(rp.get_name(hex(i['Data0_Addr'])))
            userk_out[userk_counter + 1]['Data'] = i['Data0']

            userk_counter = userk_counter + 2

        if AuroraKWord == 1:
            userk_out[userk_counter]['Address'] = i['Data1_Addr']
            userk_out[userk_counter]['Name'] = str(
                rp.get_name(hex(i['Data1_Addr'])))
            userk_out[userk_counter]['Data'] = i['Data1']

            userk_counter = userk_counter + 1

        if AuroraKWord == 2:
            userk_out[userk_counter]['Address'] = i['Data0_Addr']
            userk_out[userk_counter]['Name'] = str(
                rp.get_name(hex(i['Data0_Addr'])))
            userk_out[userk_counter]['Data'] = i['Data0']

            userk_counter = userk_counter + 1

    userk_out = userk_out[:userk_counter]

    for i in userk_out:
        logger.debug('Address= %s \t\t\tData= %s \t\t\tName= %s ', hex(
            i['Address']), hex(i['Data']), str(rp.get_name(hex(i['Address']))))

    return userk_out


def init_outs(n_hits, n_scan_params):
    hist_occ = np.zeros(shape=(400, 192), dtype=np.uint32)
    hist_tot = np.zeros(shape=(16), dtype=np.uint32)
    hist_rel_bcid = np.zeros(shape=(32), dtype=np.uint32)
    hist_scurve = np.zeros(shape=(76800, n_scan_params), dtype=np.uint32)
    hist_event_status = np.zeros(shape=(32), dtype=np.uint32)
    hist_bcid_error = np.zeros(shape=(32), dtype=np.uint32)
    hits = np.zeros(shape=n_hits,
                    dtype={'names': ['event_number', 'trigger_id', 'bcid',
                                     'rel_bcid', 'col', 'row', 'tot',
                                     'scan_param_id', 'trigger_tag',
                                     'event_status'],
                           'formats': ['int64', 'uint8', 'uint16', 'uint8',
                                       'uint16', 'uint16', 'uint8', 'uint8',
                                       'uint8', 'uint32']})

    return hits, hist_occ, hist_tot, hist_rel_bcid, hist_scurve, hist_event_status, hist_bcid_error


@numba.njit(locals={'cluster_shape': numba.int64})
def calc_cluster_shape(cluster_array):
    '''Boolean 8x8 array to number.
    '''
    cluster_shape = 0
    indices_x, indices_y = np.nonzero(cluster_array)
    for index in np.arange(indices_x.size):
        cluster_shape += 2**xy2d_morton(indices_x[index], indices_y[index])
    return cluster_shape


@numba.njit(numba.int64(numba.uint32, numba.uint32))
def xy2d_morton(x, y):
    ''' Tuple to number.

    See: https://stackoverflow.com/questions/30539347/
         2d-morton-code-encode-decode-64bits
    '''
    x = (x | (x << 16)) & 0x0000FFFF0000FFFF
    x = (x | (x << 8)) & 0x00FF00FF00FF00FF
    x = (x | (x << 4)) & 0x0F0F0F0F0F0F0F0F
    x = (x | (x << 2)) & 0x3333333333333333
    x = (x | (x << 1)) & 0x5555555555555555

    y = (y | (y << 16)) & 0x0000FFFF0000FFFF
    y = (y | (y << 8)) & 0x00FF00FF00FF00FF
    y = (y | (y << 4)) & 0x0F0F0F0F0F0F0F0F
    y = (y | (y << 2)) & 0x3333333333333333
    y = (y | (y << 1)) & 0x5555555555555555

    return x | (y << 1)


def imap_bar(func, args, n_processes=None):
    ''' Apply function (func) to interable (args) with progressbar
    '''
    p = mp.Pool(n_processes)
    res_list = []
    pbar = tqdm(total=len(args))
    for _, res in enumerate(p.imap(func, args)):
        pbar.update()
        res_list.append(res)
    pbar.close()
    p.close()
    p.join()
    return res_list


def get_threshold(x, y, n_injections, invert_x=False):
    ''' Fit less approximation of threshold from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''
    if invert_x:
        x = x[::-1].copy()
    M = y.sum(axis=len(y.shape) - 1)
    d = np.diff(x)[0]
    if invert_x:
        return x.min() + d * M / n_injections
    return x.max() - d * M / n_injections


def get_noise(x, y, n_injections, invert_x=False):
    ''' Fit less approximation of noise from s-curve.

        From: https://doi.org/10.1016/j.nima.2013.10.022

        Parameters
        ----------
        x, y : numpy array like
            Data in x and y
        n_injections: integer
            Number of injections
    '''

    if invert_x:
        x = x[::-1].copy()
    qmax = x.max()
    M = y.sum()
    d = np.diff(x)[0]
    mu = qmax - d * M / n_injections
    mu1 = y[x < mu].sum()
    mu2 = (n_injections - y[x > mu]).sum()
    return d * (mu1 + mu2) / n_injections * np.sqrt(np.pi / 2.)


def fit_scurve(scurve_data, scan_param_range, n_injections, sigma_0, invert_x):
    '''
        Fit one pixel data with Scurve.
        Has to be global function for the multiprocessing module.

        Returns:
            (mu, sigma, chi2/ndf)
    '''
    scurve_data = np.array(scurve_data)
    # Only fit data that is fittable
    if np.all(scurve_data == 0):
        return (0., 0., 0.)
    if scurve_data.max() < 0.2 * n_injections:
        return (0., 0., 0.)

    # Calculate data errors, Binomial errors
    yerr = np.sqrt(scurve_data *
                   (1. - scurve_data.astype(np.float) / n_injections))
    # Set minimum error != 0, needed for fit minimizers
    # Set arbitrarly to error of 0.5 injections
    min_err = np.sqrt(0.5 - 0.5 / n_injections)
    yerr[yerr < min_err] = min_err
    # Additional hits not following fit model set high error
    sel_bad = scurve_data > n_injections
    yerr[sel_bad] = (scurve_data - n_injections)[sel_bad]

    # Calculate threshold start value:
    mu = get_threshold(x=scan_param_range,
                       y=scurve_data,
                       n_injections=n_injections,
                       invert_x=invert_x)

    # Set fit start values
    p0 = [n_injections, mu, sigma_0]
    try:
        if invert_x:
            popt = curve_fit(f=zcurve, xdata=scan_param_range,
                             ydata=scurve_data, p0=p0, sigma=yerr,
                             absolute_sigma=True if np.any(yerr) else False)[0]
            chi2 = np.sum((scurve_data - zcurve(scan_param_range, *popt))**2)
        else:
            popt = curve_fit(f=scurve, xdata=scan_param_range,
                             ydata=scurve_data, p0=p0, sigma=yerr,
                             absolute_sigma=True if np.any(yerr) else False,
                             method='lm')[0]
            chi2 = np.sum((scurve_data - scurve(scan_param_range, *popt))**2)
    except RuntimeError:  # fit failed
        return (0., 0., 0.)

    # Treat data that does not follow an S-Curve, every fit result is possible
    # here but not meaningful
    if not invert_x:
        max_threshold = scan_param_range[-1] + 0.5 * (scan_param_range[-1] -
                                                      scan_param_range[0])
    else:
        max_threshold = scan_param_range[0] + 0.5 * (scan_param_range[0] -
                                                     scan_param_range[-1])
    if popt[1] < 0 or popt[2] <= 0 or popt[1] > max_threshold:
        return (0., 0., 0.)

    return (popt[1], popt[2], chi2 / (scurve_data.shape[0] - 3 - 1))


def fit_scurves_multithread(scurves, scan_param_range,
                            n_injections=None, invert_x=False):
    logger.info("Start S-curve fit on %d CPU core(s)", mp.cpu_count())

    scurves = np.array(scurves)
    scan_param_range = np.array(scan_param_range)

    # Calculate noise median for fit start value
    sigmas = []
    for curve in scurves:
            # TODO: n_injections is not defined, can this happen?
        if not n_injections:
            n_injections = curve.max()
        # Calculate from pixels with valid data (maximum = n_injections)
        if curve.max() == n_injections:
            sigma = get_noise(x=scan_param_range[::-1] if invert_x else scan_param_range,
                              y=curve,
                              n_injections=n_injections)
            sigmas.append(sigma)
    sigma_0 = np.median(sigmas)

    partialfit_scurve = partial(fit_scurve,
                                scan_param_range=scan_param_range,
                                n_injections=n_injections,
                                sigma_0=sigma_0,
                                invert_x=invert_x)

    result_list = imap_bar(partialfit_scurve,
                           scurves.tolist())
    result_array = np.array(result_list)
    logger.info("S-curve fit finished")

    thr = result_array[:, 0]
    sig = result_array[:, 1]
    chi2ndf = result_array[:, 2]
    thr2D = np.reshape(thr, (400, 192))
    sig2D = np.reshape(sig, (400, 192))
    chi2ndf2D = np.reshape(chi2ndf, (50 * 8, 192))
    return thr2D, sig2D, chi2ndf2D


def print_raw_data(raw_data):
    ''' Print raw data with interpretation for debugging '''
    for i, word in enumerate(raw_data):
        if word & USERK_FRAME_ID:
            print('U-K {:032b}'.format(*[word]))
            continue

        if word & TRIGGER_ID:
            print('TRG {:<26} {:032b}'.format(*[word & TRG_MASK, word]))
            continue

        if word & HEADER_ID:  # data header
            data_header = True
            fe_high_word = True

        # Reassemble full 32-bit FE data word from two FPGA data words
        # that store 16-bit data in the low word
        if fe_high_word:
            data_word = word & 0xffff
            fe_high_word = False  # Next is low word
            continue  # Low word stll missing
        else:
            data_word = data_word << 16 | word & 0xffff
            fe_high_word = True  # Next is high word

        if data_header:
            # After data header follows heder less hit data
            data_header = False

            bcid = data_word & 0x7fff
            trig_id = (data_word >> 20) & 0x1f
            trig_tag = (data_word >> 15) & 0x1f

            print('EH  {:>8} {:>8} {:>8} {:032b}'.format(
                *[bcid, trig_id, trig_tag, data_word]))
        else:  # data word is hit data
            multicol = (data_word >> 26) & 0x3f
            region = (data_word >> 16) & 0x3ff
            print('HIT ', end='')
            for i in range(4):
                col, row = translate_mapping(multicol, region, i)
                tot = (data_word >> i * 4) & 0xf

                if col < 400 and row < 192:
                    if tot != 255 and tot != 15:
                        print('{:>8} {:>8} {:>8}'.format(
                            *[col, row, tot]), end='')
                else:
                    print('UNKNOWN')
            print(' {:032b}'.format(data_word))


# Entry point for multi processing under windows
if __name__ == '__main__':
    pass
