#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script connects bdaq53 to the EUDAQ 1.7 data acquisition system.
'''

import argparse
import logging
import os
import time
import sys

import numpy as np
import tables as tb
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis.online_monitor import start_bdaq53_monitor
from bdaq53.analysis import analysis_utils as au

logger = logging.getLogger('EUDAQ Producer')

chip_configuration = 'default_chip.yaml'

local_configuration = {
}


class EudaqScan(ScanBase):
    scan_id = "eudaq_scan"

    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192,
             mask_step=4, column_step=8, n_injections=100, **kwarg):
        pass

    def analyze(self, create_pdf=True):
        pass


def replay_triggered_data(data_file, real_time=True):
    ''' Yield raw data for every trigger.

        real_time: boolean
            Delays return if replay is too fast to keep
            replay speed at original data taking speed.
    '''

    with tb.open_file(data_file, mode="r") as in_file_h5:
        meta_data = in_file_h5.root.meta_data[:]
        raw_data = in_file_h5.root.raw_data
        n_readouts = meta_data.shape[0]

        last_readout_time = time.time()

        # Leftover data from last readout
        last_readout_data = np.array([], dtype=np.uint32)
        last_trigger = -1

        for i in tqdm(range(n_readouts)):
            # Raw data indeces of readout
            i_start = meta_data['index_start'][i]
            i_stop = meta_data['index_stop'][i]

            t_start = meta_data[i]['timestamp_start']

            # Determine replay delays
            if i == 0:  # Initialize on first readout
                last_timestamp_start = t_start
            now = time.time()
            delay = now - last_readout_time
            additional_delay = t_start - last_timestamp_start - delay
            if real_time and additional_delay > 0:
                # Wait if send too fast, especially needed when readout was
                # stopped during data taking (e.g. for mask shifting)
                time.sleep(additional_delay)
            last_readout_time = time.time()
            last_timestamp_start = t_start

            actual_data = np.concatenate((last_readout_data, raw_data[i_start:i_stop]))
            trg_idx = np.where(actual_data & au.TRIGGER_ID > 0)[0]
            trigger_data = np.split(actual_data, trg_idx)

            # Special case: last readout, do not keep data for next readout
            if i == n_readouts - 1:
                trigger_data += [trigger_data[-1]]

            for dat in trigger_data[:-1]:
                if np.any(dat):
                    trigger = dat[0] & au.TRG_MASK
                    if last_trigger > 0 and trigger != last_trigger + 1:
                        logging.warning('Expected != Measured trigger number: %d != %d', last_trigger + 1, trigger)
                    last_trigger = dat[0] & au.TRG_MASK

                yield dat

            last_readout_data = trigger_data[-1]


def main():
    # Parse program arguments
    description = "Start EUDAQ producer for bdaq53"
    parser = argparse.ArgumentParser(prog='bdaq53_eudaq',
                                     description=description,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('address', metavar='address',
                        help='Destination address',
                        default='tcp://localhost:44000',
                        nargs='?')
    parser.add_argument('--path', type=str,
                        help='Absolute path of your eudaq installation')
    parser.add_argument('--replay', type=str,
                        help='Raw data file to replay for testing')
    parser.add_argument('--delay', type=float,
                        help='Additional delay when replaying data in seconds')
    args = parser.parse_args()

    # Import EUDAQ python wrapper with error handling
    try:
        from PyEUDAQWrapper import PyProducer
    except ImportError:
        if not args.path:
            logger.error('Cannot find PyEUDAQWrapper! '
                         'Please specify the path of your EUDAQ installation!')
            return
        else:
            wrapper_path = os.path.join(args.path, 'python/')
            sys.path.append(os.path.join(args.path, 'python/'))
            try:
                from PyEUDAQWrapper import PyProducer
            except ImportError:
                logger.error('Cannot find PyEUDAQWrapper in %s', wrapper_path)
                return

    logger.info('Connect to %s', args.address)

    if args.replay:
        if os.path.isfile(args.replay):
            logger.info('Replay %s', args.replay)
        else:
            logger.error('Cannot open %s for replay!', args.replay)
    delay = args.delay if args.delay else 0.

    pp = PyProducer("PyBAR", args.address)

    # Start state mashine, keep connection until termination of euRun
    while not pp.Error and not pp.Terminating:
        # Wait for configure cmd from RunControl
        while not pp.Configuring and not pp.Terminating:
            if pp.StartingRun:
                break
            time.sleep(0.1)

        # Check if configuration received
        if pp.Configuring:
            logger.info('Configuring...')
            time.sleep(3)
#             print pp.GetConfigParameter('ConfParameter')
    #         for item in run_conf:
    #             try:
    #                 run_conf[item] = pp.GetConfigParameter(item)
    #             except:
    #                 pass
#             rmngr = RunManager('../configuration.yaml')  # TODO: get conf from EUDAQ
            pp.Configuring = True

        # Check for start of run cmd from RunControl
        while not pp.StartingRun and not pp.Terminating:
            if pp.Configuring:
                break
            time.sleep(0.1)

        # Check if we are starting:
        if pp.StartingRun:
            logger.info('Starting run...')
#             join = rmngr.run_run(EudaqExtTriggerScan, run_conf=run_conf, use_thread=True)
#             join = rmngr.run_run(EudaqExtTriggerScan, use_thread=True)
            pp.StartingRun = True  # set status and send BORE

            if not args.replay:
                # Run loop
                while True:
                    if pp.Error or pp.Terminating:
                        break
                    if pp.StoppingRun:
                        break
                    dat = np.zeros(shape=(10, ), dtype=np.uint32)
                    pp.SendEvent(dat)
                    time.sleep(0.1)
            else:
                # Run loop to replay data
                for raw_data in replay_triggered_data(data_file=args.replay):
                    pp.SendEvent(raw_data)
                    if pp.Error or pp.Terminating:
                        break
                    if pp.StoppingRun:
                        break
                    time.sleep(delay)

            # Abort conditions
            if pp.Error or pp.Terminating:
                pp.StoppingRun = False  # Set status and send EORE
            # Check if the run is stopping regularly
            if pp.StoppingRun:
                pp.StoppingRun = True  # Set status and send EORE

        # Back to check for configured + start run state
        time.sleep(0.1)


if __name__ == "__main__":
    # When run in development environment, eudaq path can be added with:
    sys.path.append('/home/user/git/eudaq/python/')
    main()
