#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test writes random data into the specified registers,
    then reads back the data and compares read and written data.
'''

import random
import yaml

import tables as tb

from bdaq53.scan_base import ScanBase
from bdaq53.register_utils import RD53ARegisterParser
from bdaq53.analysis import analysis_utils


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'              : 1.25,
    'VDDD'              : 1.25,
    'VDDA_cur_lim'      : 0.7,
    'VDDD_cur_lim'      : 0.7,
    
    # Scan parameters
    'addresses' : [1, 2, 3, 4, 5, 6, 7, 8, 9,10,
                   11,12,13,14,15,16,17,18,19,20,
                   21,22,23,24,25,26,27,28,29,30,
                   31,32,33   ,35   ,37,38,39,
                   41,42,43,44   ,46,47,48,49,50,
                   51,52,53,54,55,56,57,58,59
                     ,62,
                                  76,77,78,79,80,
                   81,82,83,84,85,86,87,88,89,90,
                   91,92,93,94,95,96,97,98,99,100,
                       105,106,107,108,109,135]}


class RegisterTest(ScanBase):
    scan_id = "register_test"

    rw_registers = [1, 2, 3, 4, 5, 6, 7, 8, 9,10,
                   11,12,13,14,15,16,17,18,19,20,
                   21,22,23,24,25,26,27,28,29,30,
                   31,32,33   ,35   ,37,38,39,
                   41,42,43,44   ,46,47,48,49,50,
                   51,52,53,54,55,56,57,58,59
                     ,62,
                                  76,77,78,79,80,
                   81,82,83,84,85,86,87,88,89,90,
                   91,92,93,94,95,96,97,98,99,100,
                          105,106,107,108,109,135]

    
    def configure(self, **kwargs):
        super(RegisterTest, self).configure(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()
    
    
    def scan(self, addresses=[1], **kwargs):
        '''
        Register test main loop

        Parameters
        ----------
        addresses : list
            List of register addresses to test
        '''
        
        self.addresses = addresses
        self.rp = RD53ARegisterParser()
        self.values = {}
        self.results = {}
        
        self.logger.info('Starting scan...')
        with self.readout(fill_buffer=True, clear_buffer=True):
            for address in self.addresses:
                if not address in self.rw_registers:
                    raise ValueError('Register at address %i is not a writable register!' % (address))

                size = self.rp.get_size(address)
                value = int(random.getrandbits(size))
                self.values[address] = value

                self.logger.debug('Writing random data %s to register %s at address %s' % (
                bin(value), self.rp.get_name(address), hex(address)))
                self.chip.write_register(register=address, data=value, write=True)
                self.chip.write_command(self.chip.write_sync(write=False)*10)
                self.chip.read_register(register=address, write=True)
                self.chip.write_command(self.chip.write_sync(write=False) * 300)
        self.logger.info('Scan finished')


    def analyze(self):
        self.logger.info('Comparing data...')
        with tb.open_file(self.output_filename + '.h5', 'r+') as out_file_h5:
            raw_data = out_file_h5.root.raw_data[:]
            userk_data = analysis_utils.process_userk(analysis_utils.interpret_userk_data(raw_data))
            
            if len(userk_data) == 0:
                raise IOError('Received no data from the chip!')

            for elements in userk_data:
                self.results[elements[0]] = elements[2]
            for address in self.addresses:
                if self.results[address] != self.values[address]:
                    self.logger.error('Register %s at address %s read back a wrong value!' % (self.rp.get_name(address), hex(address)))
                    raise ValueError('Register %s at address %s read back a wrong value!' % (self.rp.get_name(address), hex(address)))
            self.logger.info('Successfully tested %i registers.' % (len(self.addresses)))
            return True


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = RegisterTest()
    scan.start(**configuration)
    scan.analyze()
