#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This script scans over different amounts of injected charge
    to find the effective threshold of the enabled pixels.
'''

import yaml
from tqdm import tqdm
import numpy as np

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'              : 1.25,
    'VDDD'              : 1.25,
    'VDDA_cur_lim'      : 0.7,
    'VDDD_cur_lim'      : 0.7,
    
    # Scan parameters
    'start_column'      : 128,
    'stop_column'       : 264,
    'start_row'         : 0,
    'stop_row'          : 192,
    'maskfile'          : 'auto',
    
    'VCAL_MED'          : 500,
    'VCAL_HIGH_start'   : 700,
    'VCAL_HIGH_stop'    : 1200,
    'VCAL_HIGH_step'    : 10
    }


class ThresholdScan(ScanBase):
    scan_id = "threshold_scan"
    
                
    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=4, column_step=8, n_injections=100,
             VCAL_MED=500, VCAL_HIGH_start=1000, VCAL_HIGH_stop=4000, VCAL_HIGH_step=100, **kwargs):
        '''
        Threshold scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        column_step : int
            Column-stepsize of the injection mask. Every column_stepth row is injected at once, up to a maximum of 8.
        n_injections : int
            Number of injections.
            
        VCAL_MED : int
            VCAL_MED DAC value.
        VCAL_HIGH_start : int
            First VCAL_HIGH value to scan.
        VCAL_HIGH_stop : int
            VCAL_HIGH value to stop the scan. This value is excluded from the scan.
        VCAL_HIGH_step : int
            VCAL_HIGH interval.
        '''
        
        vcal_high_range = range(VCAL_HIGH_start, VCAL_HIGH_stop, VCAL_HIGH_step)
        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step, column_step)
        
        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(mask_data)*len(vcal_high_range))
        for scan_param_id, vcal_high in enumerate(vcal_high_range):
            self.chip.setup_analog_injection(vcal_high=vcal_high, vcal_med=VCAL_MED)
            with self.readout(scan_param_id=scan_param_id):
                for mask in mask_data:
                    self.chip.write_command(mask['command'])
                    if mask['flavor'] == 0:
                        self.chip.inject_analog_single(send_ecr=True, repetitions=n_injections)
                    else:
                        self.chip.inject_analog_single(repetitions=n_injections)
                    pbar.update(1)
                    
        pbar.close()
        self.logger.info('Scan finished')
            
    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()
            mean_thr = np.median(a.threshold_map[np.nonzero(a.threshold_map)])
            try: self.logger.info('Mean threshold is %i [Delta VCAL]' % (int(mean_thr)))
            except: self.logger.error('Mean threshold could not be determined!')
        
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()
                
        return mean_thr


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = ThresholdScan()
    scan.start(**configuration)
    scan.analyze()
