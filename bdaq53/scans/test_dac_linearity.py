#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic test scans over the values of the selected DAC and
    measures the resulting analog value with the chip's internal ADC.
'''

import yaml
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting

chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'         : 1.25,
    'VDDD'         : 1.25,
    'VDDA_cur_lim' : 0.7,
    'VDDD_cur_lim' : 0.7,

    # Scan parameters
    'DAC'          : 'VCAL_HIGH',
    'type'         : 'U',
    'value_start'  : 0,
    'value_stop'   : 4096,
    'value_step'   : 100}


class LinearityTest(ScanBase):
    scan_id = "dac_linearity_test"

    def configure(self, **kwargs):
        self.chip.set_dacs(**kwargs)
        self.chip.enable_monitor_filter()
        self.chip.enable_monitor_data()


    def scan(self, **kwargs):
        values = range(kwargs.get('value_start'), kwargs.get('value_stop'), kwargs.get('value_step'))
        address = kwargs.get('DAC')
        typ = kwargs.get('type')

        self.logger.info('Starting scan...')
        pbar = tqdm(total=len(values))
        for scan_param_id in values:
            with self.readout(scan_param_id=scan_param_id):
                self.chip.write_register(register=address, data=scan_param_id, write=True)
                self.chip.get_ADC(typ, address)
                pbar.update(1)
        
        pbar.close()
        self.logger.info('Scan finished')


    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_adc_data()
        
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.logger.info('Creating selected plots...')
                p.create_parameter_page()
                p.create_dac_linearity_plot()


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = LinearityTest()
    scan.start(**configuration)
    scan.analyze()