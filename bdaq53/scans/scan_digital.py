#
# ------------------------------------------------------------
# Copyright (c) All rights reserved
# SiLab, Institute of Physics, University of Bonn
# ------------------------------------------------------------
#

'''
    This basic scan injects a digital pulse into
    enabled pixels to test the digital part of the chip.
'''

import yaml
from tqdm import tqdm

from bdaq53.scan_base import ScanBase
from bdaq53.analysis import analysis
from bdaq53.analysis import plotting


chip_configuration = 'default_chip.yaml'

local_configuration = {
    # Hardware settings
    'VDDA'         : 1.25,
    'VDDD'         : 1.25,
    'VDDA_cur_lim' : 0.7,
    'VDDD_cur_lim' : 0.7,
    
    # Scan parameters
    'start_column' : 0,
    'stop_column'  : 400,
    'start_row'    : 0,
    'stop_row'     : 192,
    'maskfile'     : 'auto'
    }

class DigitalScan(ScanBase):
    scan_id = "digital_scan"


    def scan(self, start_column=0, stop_column=400, start_row=0, stop_row=192, mask_step=4, column_step=8, n_injections=100, **kwargs):
        '''
        Digital scan main loop

        Parameters
        ----------
        start_column : int [0:400]
            First column to scan
        stop_column : int [0:400]
            Column to stop the scan. This column is excluded from the scan.
        start_row : int [0:192]
            First row to scan
        stop_row : int [0:192]
            Row to stop the scan. This row is excluded from the scan.
        mask_step : int
            Row-stepsize of the injection mask. Every mask_stepth row is injected at once.
        column_step : int
            Column-stepsize of the injection mask. Every column_stepth row is injected at once, up to a maximum of 8.
        n_injections : int
            Number of injections.
        '''
        
        self.logger.info('Preparing injection masks...')
        mask_data = self.prepare_injection_masks(start_column, stop_column, start_row, stop_row, mask_step, column_step)
#         mask_data = self.prepare_logo_mask(image='../logo.bmp')
        
        self.logger.info('Starting scan...')     
        pbar = tqdm(total=len(mask_data)) 
        with self.readout():
            for mask in mask_data:
                self.chip.write_command(mask['command'])
                self.chip.inject_digital(repetitions=n_injections)
                pbar.update(1)
                    
        pbar.close()
        self.logger.info('Scan finished')


    def analyze(self, create_pdf=True):
        with analysis.Analysis(raw_data_file=self.output_filename + '.h5') as a:
            a.analyze_data()
        
        if create_pdf:
            with plotting.Plotting(analyzed_data_file=a.analyzed_data_file) as p:
                p.create_standard_plots()


if __name__ == "__main__":
    with open(chip_configuration, 'r') as f:
        configuration = yaml.load(f)
    configuration.update(local_configuration)
    
    scan = DigitalScan()
    scan.start(**configuration)
    scan.analyze()
