/**
 * ------------------------------------------------------------
 * Copyright (c) All rights reserved
 * SiLab, Institute of Physics, University of Bonn
 * ------------------------------------------------------------
 */

`timescale 1ns/1ps
`default_nettype none

`ifndef SYNTHESIS
    `include "rx_aurora/rx_aurora_64b66b_1lane/ip/exdes/aurora_64b66b_1lane_exdes.v"
`endif


module rx_aurora_64b66b_core
#(
    parameter ABUSWIDTH = 16,
    parameter IDENTIFIER = 0,
    parameter AURORA_LANES = 1
)(
    input wire [3:0] RXP, RXN,
    output wire TX_P, TX_N,
    input wire RX_CLK_IN_P, RX_CLK_IN_N,
    input wire INIT_CLK_IN_P, INIT_CLK_IN_N,
    output wire REFCLK1_OUT,
    output wire TX_OUT_CLK,

    input wire FIFO_READ,
    output wire FIFO_EMPTY,
    output wire [31:0] FIFO_DATA,   //output format #ID (IDENTIFIER + 1 frame start + 16 bit data)

    input wire USERK_FIFO_READ,
    output wire USERK_FIFO_EMPTY,
    output wire [31:0] USERK_FIFO_DATA,

    input wire BUS_CLK,
    input wire [ABUSWIDTH-1:0] BUS_ADD,
    input wire [7:0] BUS_DATA_IN,
    output reg [7:0] BUS_DATA_OUT,
    input wire BUS_RST,
    input wire BUS_WR,
    input wire BUS_RD,

    output wire LOST_ERROR,
    output wire RX_LANE_UP,
    output wire RX_CHANNEL_UP,
    output wire PLL_LOCKED,
    output wire MGT_REF_SEL,

    input wire CLK_CMD,
    input wire CMD_DATA,
    inout wire CMD_OUTPUT_EN,

    input wire AURORA_RESET,

    output wire RX_SOFT_ERROR, RX_HARD_ERROR
);

localparam VERSION = 2;


wire SOFT_RST;
assign SOFT_RST = (BUS_ADD==0 && BUS_WR);

wire RST;       // global module reset
assign RST = BUS_RST | SOFT_RST;

reg RESET_LOGIC;    // reset only the logic and the fifos
wire RESET_LOGIC_pulse;
pulse_gen_rising reset_fsm (.clk_in(BUS_CLK), .in(RESET_LOGIC), .out(RESET_LOGIC_pulse));


wire USER_CLK;
wire INIT_CLK_P, INIT_CLK_N;
wire RX_CLK_P, RX_CLK_N;
assign RX_CLK_P = RX_CLK_IN_P;
assign RX_CLK_N = RX_CLK_IN_N;
assign INIT_CLK_P = INIT_CLK_IN_P;
assign INIT_CLK_N = INIT_CLK_IN_N;


reg CONF_EN ;
reg CONF_SI570_SET = 0;
reg CONF_USER_K_FILTER_EN;
reg [7:0] USER_K_FILTER_MASK_1, USER_K_FILTER_MASK_2, USER_K_FILTER_MASK_3;
reg [7:0] LOST_DATA_CNT = 8'd0;
reg [31:0] FRAME_COUNTER = 32'd0;
reg RESET_COUNTERS;
wire RESET_COUNTERS_pulse;
//wire RX_HARD_ERROR, RX_SOFT_ERROR;
wire RX_READY;
reg CONF_MGT_REF_SEL;
assign MGT_REF_SEL = CONF_MGT_REF_SEL;
reg GTX_TX_MODE;

always @(posedge BUS_CLK) begin
    if(RST) begin
        CONF_EN <= 1;
        CONF_USER_K_FILTER_EN <= 1;
        CONF_MGT_REF_SEL <= 1;
        USER_K_FILTER_MASK_1 <= 8'h01;
        USER_K_FILTER_MASK_2 <= 8'h02;
        USER_K_FILTER_MASK_3 <= 8'h04;
        RESET_COUNTERS <= 0;
        RESET_LOGIC <= 0;
        GTX_TX_MODE <= 0;
    end
    else if(BUS_WR) begin
        if(BUS_ADD == 2) begin
            CONF_EN <= BUS_DATA_IN[0];
            CONF_MGT_REF_SEL <= BUS_DATA_IN[6];
            CONF_USER_K_FILTER_EN <= BUS_DATA_IN[7];
        end
        else if(BUS_ADD == 4)
            USER_K_FILTER_MASK_1  <= BUS_DATA_IN;
        else if(BUS_ADD == 5)
            USER_K_FILTER_MASK_2  <= BUS_DATA_IN;
        else if(BUS_ADD == 6)
            USER_K_FILTER_MASK_3  <= BUS_DATA_IN;
        else if(BUS_ADD == 7) begin
            RESET_COUNTERS <= BUS_DATA_IN[0];
            RESET_LOGIC <= BUS_DATA_IN[1];
            CONF_SI570_SET <= BUS_DATA_IN[2];
            GTX_TX_MODE <= BUS_DATA_IN[3];
        end
    end
end


// reset pulse for the (error) counters
pulse_gen_rising reset_counters (.clk_in(BUS_CLK), .in(RESET_COUNTERS), .out(RESET_COUNTERS_pulse));

// count the sent data packaged (in units of 32 bits)
always@(posedge BUS_CLK) begin
    if(FIFO_READ & !FIFO_EMPTY & (FRAME_COUNTER < 24'hffffff))
        FRAME_COUNTER <= FRAME_COUNTER + 1;
    if (RESET_COUNTERS_pulse)
        FRAME_COUNTER <= 32'b0;
end
/*
 * // count the number of received aurora packages (in units of 64 bit)
always@(posedge USER_CLK) begin
    if(RX_TVALID & (FRAME_COUNTER < 24'hffffff))
        FRAME_COUNTER <= FRAME_COUNTER + 1;
    if (RESET_COUNTERS_pulse | RST | RESET_LOGIC_pulse)
        FRAME_COUNTER <= 32'b0;
end
*/

// count soft errors (count rising edged)
reg [7:0] RX_SOFT_ERROR_COUNTER;
reg RX_SOFT_ERROR_reg;
wire RX_SOFT_ERROR_SYNC;
cdc_pulse_sync cdc_soft_error (.clk_in(USER_CLK), .pulse_in(RX_SOFT_ERROR), .clk_out(BUS_CLK), .pulse_out(RX_SOFT_ERROR_SYNC));
always@(posedge BUS_CLK) begin
    if (RESET_COUNTERS_pulse | RST | RESET_LOGIC_pulse)
        RX_SOFT_ERROR_COUNTER <= 32'b0;
    else if(RX_SOFT_ERROR_SYNC & !RX_SOFT_ERROR_reg & (RX_SOFT_ERROR_COUNTER < 8'hff))
        RX_SOFT_ERROR_COUNTER <= RX_SOFT_ERROR_COUNTER + 1;
    RX_SOFT_ERROR_reg <= RX_SOFT_ERROR_SYNC;
end


// count hard errors (count rising edged)
reg [7:0] RX_HARD_ERROR_COUNTER;
reg RX_HARD_ERROR_reg;
wire RX_HARD_ERROR_SYNC;
cdc_pulse_sync cdc_hard_error (.clk_in(USER_CLK), .pulse_in(RX_HARD_ERROR), .clk_out(BUS_CLK), .pulse_out(RX_HARD_ERROR_SYNC));
always@(posedge BUS_CLK) begin
    if (RESET_COUNTERS_pulse | RST | RESET_LOGIC_pulse)
        RX_HARD_ERROR_COUNTER <= 32'b0;
    else if(RX_HARD_ERROR_SYNC & !RX_HARD_ERROR_reg & (RX_HARD_ERROR_COUNTER < 8'hff))
        RX_HARD_ERROR_COUNTER <= RX_HARD_ERROR_COUNTER + 1;
    RX_HARD_ERROR_reg <= RX_HARD_ERROR_SYNC;
end


always @(posedge BUS_CLK) begin
    if(BUS_RD) begin
        if(BUS_ADD == 0)
            BUS_DATA_OUT <= VERSION;
        else if(BUS_ADD == 2)
                BUS_DATA_OUT <= {CONF_USER_K_FILTER_EN, MGT_REF_SEL, RX_HARD_ERROR_SYNC&PLL_LOCKED, RX_SOFT_ERROR_SYNC&PLL_LOCKED, PLL_LOCKED, RX_LANE_UP, RX_READY, CONF_EN};
        else if(BUS_ADD == 3)
            BUS_DATA_OUT <= LOST_DATA_CNT;
        else if(BUS_ADD == 4)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_1;
        else if(BUS_ADD == 5)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_2;
        else if(BUS_ADD == 6)
            BUS_DATA_OUT <= USER_K_FILTER_MASK_3;
        else if(BUS_ADD == 7)
            BUS_DATA_OUT <= {4'b0, GTX_TX_MODE, CONF_SI570_SET, RESET_LOGIC, RESET_COUNTERS};
        else if(BUS_ADD == 8)
            BUS_DATA_OUT <= FRAME_COUNTER[7:0];
        else if(BUS_ADD == 9)
            BUS_DATA_OUT <= FRAME_COUNTER[15:8];
        else if(BUS_ADD == 10)
            BUS_DATA_OUT <= FRAME_COUNTER[23:16];
        else if(BUS_ADD == 11)
            BUS_DATA_OUT <= FRAME_COUNTER[31:24];
        else if(BUS_ADD == 12)
            BUS_DATA_OUT <= RX_SOFT_ERROR_COUNTER[7:0];
        else if(BUS_ADD == 13)
            BUS_DATA_OUT <= RX_HARD_ERROR_COUNTER[7:0];
      else
            BUS_DATA_OUT <= 8'b0;
    end
end


wire CONF_EN_SYNC;
cdc_reset_sync rst_conf_en_sync (.clk_in(BUS_CLK), .pulse_in(CONF_EN), .clk_out(USER_CLK), .pulse_out(CONF_EN_SYNC));

// Aurora init
reg pma_init_r;
reg [4:0] pma_init_cnt;
always@ (posedge BUS_CLK) begin
    `ifdef USBPix3
        if (AURORA_RESET | RST)     // reset only once if the GTX transmitter is used, in order to not lose the link?
    `else
        if (AURORA_RESET | RST)
    `endif
            pma_init_cnt <= 5'hf;
    if (pma_init_cnt > 5'h0) begin
        pma_init_r <= 1'b1;
        pma_init_cnt <= pma_init_cnt - 1;
    end
    else
       pma_init_r <= 1'b0;
end

wire RX_TLAST;
wire RX_TVALID;
wire [63:0] RX_TDATA;
wire [7:0] RX_TKEEP;

assign RX_READY = RX_CHANNEL_UP & RX_LANE_UP;

wire RST_USER_SYNC;
cdc_reset_sync rst_pulse_user_sync (.clk_in(BUS_CLK), .pulse_in(RST), .clk_out(USER_CLK), .pulse_out(RST_USER_SYNC));

reg RX_TFIRST;
always@(posedge USER_CLK)
    if(RST_USER_SYNC)
        RX_TFIRST <= 1;
    else if(RX_TVALID & RX_TLAST)
        RX_TFIRST <= 1;
    else if(RX_TVALID)
        RX_TFIRST <= 0;


localparam DATA_SIZE_FIFO = 1+1+64;
wire byte4;
assign byte4 = (RX_TKEEP == 8'hff);
wire [DATA_SIZE_FIFO-1:0] data_to_cdc;
assign data_to_cdc = {byte4, RX_TFIRST, RX_TDATA};

wire [DATA_SIZE_FIFO-1:0] cdc_data_out;
wire read_fifo_cdc;
wire fifo_full;
wire write_out_fifo;
wire wfull, cdc_fifo_empty;
reg [1:0] byte2_cnt, byte2_cnt_prev;
reg [1:0] userk_byte2_cnt, userk_byte2_cnt_prev;

assign write_out_fifo = (byte2_cnt != 0 || byte2_cnt_prev != 0);
assign read_fifo_cdc = (byte2_cnt_prev==0 & byte2_cnt!=0);

// CDC fifo for the pixel data
cdc_syncfifo #(.DSIZE(DATA_SIZE_FIFO), .ASIZE(8)) cdc_syncfifo_i
(
    .rdata(cdc_data_out),
    .wfull(wfull),
    .rempty(cdc_fifo_empty),
    .wdata(data_to_cdc),
    .winc(RX_TVALID & CONF_EN_SYNC), .wclk(USER_CLK), .wrst(RST_USER_SYNC),
    .rinc(read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST | RESET_LOGIC_pulse)
    );


localparam DATA_USERK_SIZE_FIFO = 64+1;
wire [63:0] USER_K_DATA;
wire USER_K_VALID;
wire USER_K_ERR;

wire [DATA_USERK_SIZE_FIFO-1:0] userk_data_to_cdc, userk_cdc_data_out;
wire userk_fifo_full, userk_write_out_fifo, userk_wfull, userk_cdc_fifo_empty, userk_read_fifo_cdc;

reg  USER_K_VALID_delayed;
wire USERK_RX_TFIRST_COMB;
always@(posedge USER_CLK) begin
    USER_K_VALID_delayed <= USER_K_VALID;
end

reg [63:0] TX_DATA;

// Filter to seperate monitor data from register data etc
wire USER_K_FILTER_PASSED;
assign USER_K_FILTER_PASSED = ( ( USER_K_DATA[63:56] == USER_K_FILTER_MASK_1 )
                              | ( USER_K_DATA[63:56] == USER_K_FILTER_MASK_2 )
                              | ( USER_K_DATA[63:56] == USER_K_FILTER_MASK_3 )
                              ) ? 1 : 0;

// Write to CDC fifo after applying the filter
assign USERK_RX_TFIRST_COMB = USER_K_VALID & !USER_K_VALID_delayed & (USER_K_FILTER_PASSED | ~CONF_USER_K_FILTER_EN);

assign userk_data_to_cdc = {USERK_RX_TFIRST_COMB, USER_K_DATA};
assign userk_write_out_fifo = (userk_byte2_cnt != 0 || userk_byte2_cnt_prev != 0);
assign userk_read_fifo_cdc = (userk_byte2_cnt_prev==0 & userk_byte2_cnt!=0);

// CDC fifo for the USER_K data
cdc_syncfifo #(.DSIZE(DATA_USERK_SIZE_FIFO), .ASIZE(8)) userk_cdc_syncfifo_i
(
    .rdata(userk_cdc_data_out),
    .wfull(userk_wfull),
    .rempty(userk_cdc_fifo_empty),
    .wdata(userk_data_to_cdc),
    .winc(USERK_RX_TFIRST_COMB & CONF_EN_SYNC), .wclk(USER_CLK), .wrst(RST_USER_SYNC),
    .rinc(userk_read_fifo_cdc), .rclk(BUS_CLK), .rrst(RST | RESET_LOGIC_pulse)
);


aurora_64b66b_1lane_exdes   #( .SIMPLEX_TIMER_VALUE(10) ) aurora_frame (
    // Error signals from Aurora
    .RX_HARD_ERR(RX_HARD_ERROR),
    .RX_SOFT_ERR(RX_SOFT_ERROR),
    .RX_LANE_UP(RX_LANE_UP),
    .RX_CHANNEL_UP(RX_CHANNEL_UP),
    .PLL_LOCKED(PLL_LOCKED),
    .REFCLK1_OUT(REFCLK1_OUT),
    .TX_OUT_CLK(TX_OUT_CLK),

    .INIT_CLK_P(INIT_CLK_P),
    .INIT_CLK_N(INIT_CLK_N),

    .PMA_INIT(pma_init_r),

    .GTXQ0_P(RX_CLK_P),
    .GTXQ0_N(RX_CLK_N),

    .RXP(RXP),
    .RXN(RXN),

    .TX_P(TX_P), .TX_N(TX_N),

    .TX_DATA(TX_DATA),

    // Error signals from the Local Link packet checker
    .DATA_ERR_COUNT(),
    //USER_K
    .USER_K_ERR(USER_K_ERR),
    .USER_K_DATA(USER_K_DATA),
    .USER_K_VALID(USER_K_VALID),

    // User IO
    .RESET(RST_USER_SYNC),

    .DRP_CLK_IN(USER_CLK),
    .USER_CLK(USER_CLK),
    .RX_TDATA(RX_TDATA),
    .RX_TVALID(RX_TVALID),
    .RX_TKEEP(RX_TKEEP),
    .RX_TLAST(RX_TLAST)
);


//ila_0 ila (
// .clk(USER_CLK),
// .probe0({RX_TFIRST, RX_TVALID, RX_TLAST, RX_TKEEP, RX_CHANNEL_UP, RX_LANE_UP, RX_SOFT_ERROR, RX_HARD_ERROR})
//);

// RX DATA
always@(posedge BUS_CLK)
    byte2_cnt_prev <= byte2_cnt;

always@(posedge BUS_CLK)
    if(RST | RESET_LOGIC_pulse)
        byte2_cnt <= 0;
    else if(!cdc_fifo_empty && !fifo_full && byte2_cnt == 0 )
    begin
        if(cdc_data_out[DATA_SIZE_FIFO-1])
            byte2_cnt <= 3;
        else
            byte2_cnt <= 1;
    end
    else if (!fifo_full & byte2_cnt != 0)
        byte2_cnt <= byte2_cnt - 1;

// USER DATA
always@(posedge BUS_CLK)
    userk_byte2_cnt_prev <= userk_byte2_cnt;

always@(posedge BUS_CLK)
    if(RST | RESET_LOGIC_pulse)
        userk_byte2_cnt <= 0;
    else if(!userk_cdc_fifo_empty && !userk_fifo_full && userk_byte2_cnt == 0 )
    begin
        userk_byte2_cnt <= 3;
    end
    else if (!userk_fifo_full & userk_byte2_cnt != 0)
        userk_byte2_cnt <= userk_byte2_cnt - 1;


reg [DATA_SIZE_FIFO-1:0] data_buf;
wire [16:0] data_out;
wire [16:0] fifo_data_out_byte [3:0];

wire byte4_sel;
assign byte4_sel = read_fifo_cdc ? cdc_data_out[DATA_SIZE_FIFO-1] : data_buf[DATA_SIZE_FIFO-1];

assign fifo_data_out_byte[0] = byte4_sel ? {1'b0, data_buf[23:16], data_buf[31:24]} : {1'b0, data_buf[55:48], data_buf[63:56]};
assign fifo_data_out_byte[1] = byte4_sel ? {1'b0, data_buf[7:0],data_buf[15:8]} : {cdc_data_out[DATA_SIZE_FIFO-2], cdc_data_out[39:32],cdc_data_out[47:40]};
assign fifo_data_out_byte[2] = {1'b0, data_buf[55:48], data_buf[63:56]} ;
assign fifo_data_out_byte[3] = {cdc_data_out[DATA_SIZE_FIFO-2], cdc_data_out[39:32],cdc_data_out[47:40]};


always@(posedge BUS_CLK)
    if(read_fifo_cdc)
        data_buf <= cdc_data_out;

wire [23:0] cdc_data;
assign cdc_data = {7'b0, data_out};
assign data_out = fifo_data_out_byte[byte2_cnt];
gerneric_fifo #(.DATA_SIZE(24), .DEPTH(1024*64))  fifo_i
(   .clk(BUS_CLK), .reset(RST | RESET_LOGIC_pulse),
    .write(write_out_fifo),
    .read(FIFO_READ),
    .data_in(cdc_data),
    .full(fifo_full),
    .empty(FIFO_EMPTY),
    .data_out(FIFO_DATA[23:0]),
    .size()
);

// USER K
reg [DATA_USERK_SIZE_FIFO-1:0] userk_data_buf;
always@(posedge BUS_CLK)
    if(userk_read_fifo_cdc)
        userk_data_buf <= userk_cdc_data_out;

wire [16:0] userk_fifo_data_out_byte [3:0];

assign userk_fifo_data_out_byte[0] = {1'b0,                                   userk_data_buf[23:16],     userk_data_buf[31:24]};
assign userk_fifo_data_out_byte[1] = {1'b0,                                   userk_data_buf[7:0],       userk_data_buf[15:8]};
assign userk_fifo_data_out_byte[2] = {1'b0,                                   userk_data_buf[55:48],     userk_data_buf[63:56]};
assign userk_fifo_data_out_byte[3] = {userk_cdc_data_out[DATA_SIZE_FIFO-2],   userk_cdc_data_out[39:32], userk_cdc_data_out[47:40]};


wire [16:0] userk_data_out;
wire [23:0] userk_cdc_data;
assign userk_data_out = userk_fifo_data_out_byte[userk_byte2_cnt];
assign userk_cdc_data = {7'b0, userk_data_out};

gerneric_fifo #(.DATA_SIZE(24), .DEPTH(1024))  userk_fifo_i
(   .clk(BUS_CLK), .reset(RST | RESET_LOGIC_pulse),
    .write(userk_write_out_fifo),
    .read(USERK_FIFO_READ),
    .data_in(userk_cdc_data),
    .full(userk_fifo_full),
    .empty(USERK_FIFO_EMPTY),
    .data_out(USERK_FIFO_DATA[23:0]),
    .size()
);

always@(posedge USER_CLK) begin
    if(RST_USER_SYNC)
        LOST_DATA_CNT <= 0;
    else if (wfull && RX_TVALID && LOST_DATA_CNT != -1)
        if (LOST_DATA_CNT < 8'hff)
            LOST_DATA_CNT <= LOST_DATA_CNT +1;
end

assign FIFO_DATA[31:24]       =  {IDENTIFIER[7:0]};
assign USERK_FIFO_DATA[31:24] =  {8'b0};

assign LOST_ERROR = LOST_DATA_CNT != 0;


// CONVERT COMMANDS for the 64 bit GTX running at 1.28 Gb/s
reg [2:0] cmd_sr = 0;
reg [7:0] CMD_BUFFER;
reg [63:0] TX_DATA_REG = 0;
always @ (posedge CLK_CMD) begin
    if (GTX_TX_MODE == 0) begin     // GTX in CMD mode
        CMD_BUFFER[7:1] <= CMD_BUFFER[6:0];
        CMD_BUFFER[0] <= CMD_DATA;
        if (cmd_sr == 3'd7) begin           // fill shift register for 8 CMD_CLK cycles, then expand every bit to 8 bits
            if (CMD_OUTPUT_EN)              // set cmd to zero if the output is disabled (by software)
                TX_DATA <= ~TX_DATA_REG;    // send previous data to GTX. Inverted due to PCB layout (CMD_N and CMD_P swapped)
            else
                TX_DATA <= 64'd0;
            if (CMD_BUFFER[7]) TX_DATA_REG[0*8+7:0*8] <= 8'hff; else TX_DATA_REG[0*8+7:0*8] <= 8'h00;
            if (CMD_BUFFER[6]) TX_DATA_REG[1*8+7:1*8] <= 8'hff; else TX_DATA_REG[1*8+7:1*8] <= 8'h00;
            if (CMD_BUFFER[5]) TX_DATA_REG[2*8+7:2*8] <= 8'hff; else TX_DATA_REG[2*8+7:2*8] <= 8'h00;
            if (CMD_BUFFER[4]) TX_DATA_REG[3*8+7:3*8] <= 8'hff; else TX_DATA_REG[3*8+7:3*8] <= 8'h00;
            if (CMD_BUFFER[3]) TX_DATA_REG[4*8+7:4*8] <= 8'hff; else TX_DATA_REG[4*8+7:4*8] <= 8'h00;
            if (CMD_BUFFER[2]) TX_DATA_REG[5*8+7:5*8] <= 8'hff; else TX_DATA_REG[5*8+7:5*8] <= 8'h00;
            if (CMD_BUFFER[1]) TX_DATA_REG[6*8+7:6*8] <= 8'hff; else TX_DATA_REG[6*8+7:6*8] <= 8'h00;
            if (CMD_BUFFER[0]) TX_DATA_REG[7*8+7:7*8] <= 8'hff; else TX_DATA_REG[7*8+7:7*8] <= 8'h00;
    //        for (i=0; i<8; i=i+1) begin
    //            if (CMD_DATA) TX_DATA_REG[i*8+7:i] <= 8'hff; else TX_DATA_REG[i*8+7:i] <= 8'h00;
    //        end
        end
        cmd_sr <= cmd_sr + 1;
    end
    else begin  // GTX in 640 MHz clock mode
        TX_DATA <= 64'h55555555_55555555;
    end
end

endmodule
